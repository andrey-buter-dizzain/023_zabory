<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * The template for displaying Socialflow theme footer.
 *
 * @package WordPress
 * @subpackage Socialflow
 * @since Socialflow 1.0
 */

?>

	<footer id="site-footer">
		<div class="wrapper">
			
			<div class="copyright">
				<?php echo wp_theme_copyright( 2017 ) ?> <?php echo get_theme_option( 'copyright' ) ?>
			</div>
		</div>
	</footer>

</div>

<?php wp_footer(); ?>

</body>
</html>