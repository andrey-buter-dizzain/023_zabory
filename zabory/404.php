<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage theme_name
 * @since theme_name 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area wrapper full-width">
		<div id="content" class="site-main" role="main">
					
			<?php get_theme_part( 'content', '404' ) ?>
			
		</div>
	</div>

<?php get_footer(); ?>