<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$price    = get_theme_option( 'price_link' );
$contacts = get_theme_option( 'contacts' );

?><!DOCTYPE html>
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[!(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=1200">
	<?php /* ?>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php */ ?>
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<?php do_action( 'theme_after_open_body' ) ?>

	<div id="site-header">
		<div class="wrapper">
			<div id="site-logo">
				<?php /* ?>
				<a href="<?php echo home_url() ?>" title="<?php bloginfo( 'name' ); ?>">
					<img src="<?php echo THEME_URI ?>/images/logo.svg" alt=""><?php */ ?>
					<img src="<?php echo THEME_URI ?>/images/logo-new-4.jpg" alt="">
					<?php /* ?><span><?php _e( 'от производителя' ) ?></span>
				</a>
				<?php */ ?>
			</div>

			<nav id="primary-navigation" role="navigation">
				<?php wp_nav_menu( array(
					'theme_location' => 'primary',
					'container'      => false,
				) ); ?>
			</nav>

			<?php if ( $contacts ): ?>
				<div class="header-contacts">
					<?php echo wpautop( $contacts ) ?>
				</div>
			<?php endif ?>

			<?php if ( $price ): ?>
				<a href="" download="<?php echo $price ?>" class="header-price-link"><?php _e( 'Скачать прайс' ) ?></a>
			<?php endif ?>
		</div>
	</div>

	<div id="page">
		<?php get_theme_part( 'section', 'mobile-nav-toggler' ) ?>
