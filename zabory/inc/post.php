<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Page module
 */

class Inc_Post extends Inc_Load_Section
{
	public static $section = 'post';

	protected static $include_path;

	protected static $uri_path;

	public static $partials = array(
		'meta',
	);
}
Inc_Post::load();