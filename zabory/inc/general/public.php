<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

function theme_explode_list( $text ) {
	if ( !$text )
		return array();

	$text = wpautop( $text );

	if ( preg_match_all( "/<p>([^<]*)<\/p>/", $text, $matches ) )
		return $matches[1];

	return array( $text );
}