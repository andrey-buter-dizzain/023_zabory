<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

function theme_localize_script( $data = array(), $group = '' ) {
	Theme_Scripts_Styles::add_localize_script( $data, $group );
}


/**
 * Include Theme Styles and Scripts
 *
 * @package WordPress
 */

class Theme_Scripts_Styles
{
	protected $is_localhost = true;

	// protected $min = '.min';
	protected $min = '';

	protected $theme_minimized = array(
		'common.js',
		'screen.css'
	);

	protected static $localize_script_data = array(
		'cache_key' => 'theme_localize_script',
		'name'      => 'themeData',
		'handler'   => 'empty-script-js'
	);

	protected static $cache_key = 'theme_localize_script';

	protected $exclude_defer = array(
		'smartling'
	);

	protected $use_defer = true;

	function enqueue_scripts()
	{
		// $this->enqueue_script( 'page-internal', 'page-templates/internal.js', array( 'jquery' ), false );
		
		$this->enqueue_script( 'libs-js', 'libs.js', array( 'jquery' ), false );
		$this->enqueue_script( 'common-js', 'common.js', array( 'jquery' ), false );

		//$this->enqueue_script( 'empty-script-js', 'empty-script.js', array( 'jquery' ), true );

		// Load the Internet Explorer 9 specific stylesheet.
		// $this->enqueue_script( 'html5', 'html5.js', array( 'jquery' ), false );
		// wp_script_add_data( 'html5', 'conditional', 'lt IE 9' );

		if ( is_localhost() ) {
			// wp_enqueue_script( 'live-reload', '//192.168.1.100:35729/livereload.js', false, '1', false );
			// wp_enqueue_script( 'live-reload', '//localhost:35729/livereload.js', false, '1', false );

		/* ?>
			<script>
				(function() {
				var lrHost = location.protocol + '//' + location.hostname + ':35729';
				var s = document.createElement('script');
				s.async = true;
				s.setAttribute('src', lrHost + '/livereload.js');
				document.body.appendChild(s);
				})();
			</script>
		<?php  */

			wp_add_inline_script( 'common-js', "
				(function() {
					document.addEventListener('DOMContentLoaded', ready);
					function ready() {
						var lrHost = location.protocol + '//' + location.hostname + ':35729';
						var s = document.createElement('script');
						s.async = true;
						s.setAttribute('src', lrHost + '/livereload.js');
						document.body.appendChild(s);
					}
				})();
			" );
		}

		// wp_enqueue_script( 'nrec-typekit', 'https://use.typekit.net/uwf2hys.js', array(), '1.0' );
		// wp_add_inline_script( 'nrec-typekit', 'try{Typekit.load({ async: true });}catch(e){}' );
	}

	function enqueue_styles()
	{
		// $this->enqueue_style( 'fancybox', 'libs/jquery.fancybox.css' );

		// wp_enqueue_style( 'google_fonts', '//fonts.googleapis.com/css?family=Playfair+Display:400,400italic' );
		
		$this->enqueue_style( 'theme-styles', 'screen.css' );
		$this->enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800&amp;subset=cyrillic' );
	}

	function head_scripts()
	{
		$path = $this->uri( 'images/favicon' );

		// self::print_extra_localize_script( 'themeLocalizations', $GLOBALS['localization_data'] );
		// self::print_extra_inline_script( 'include-to-html/localization.js' );
		
	?>
		<link rel="shortcut icon" href="<?php echo $path ?>/favicon.ico">

		<?php /* ?>
		<link rel="apple-touch-icon" href="<?php echo $path ?>/favicon_mac.ico">
		<link rel="apple-touch-icon" sizes="16x16" href="<?php echo $path ?>/favicon16.png" />
		<link rel="apple-touch-icon" sizes="32x32" href="<?php echo $path ?>/favicon32.png" />
		<link rel="apple-touch-icon" sizes="48x48" href="<?php echo $path ?>/favicon48.png" />
		<link rel="apple-touch-icon" sizes="64x64" href="<?php echo $path ?>/favicon64.png" />
		<link rel="apple-touch-icon" sizes="128x128" href="<?php echo $path ?>/favicon128.png" />
		<?php */ ?>

		<?php // webmaster.yandex.ru  ?>
		<meta name="yandex-verification" content="bde70f7472d97dae" />

		<?php if ( ! is_localhost()  ): ?>
			<!-- Yandex.Metrika counter --> 
			<script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter44557619 = new Ya.Metrika({ id:44557619, clickmap:true, trackLinks:true, accurateTrackBounce:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/44557619" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->

			<!-- Google Tag Manager -->
			<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-5L87FQH');</script>
			<!-- End Google Tag Manager -->
		<?php endif; ?>

	<?php 
	
	}

	function footer_scripts()
	{
	
	}
	function scripts_in_body()
	{
		if ( is_localhost() )
			return;
	?>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5L87FQH"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
	<?php 
	}

	/**
	 * Plugin Name: Enqueue jQuery in Footer
	 * Version:     0.0.1
	 * Plugin URI:  http://wpgrafie.de/836/
	 * Description: Prints jQuery in footer on front-end.
	 * Author:      Dominik Schilling
	 * Author URI:  http://wpgrafie.de/
	 */
	function enqueue_jquery_in_footer()
	{
		if ( is_admin() )
			return;

		wp_deregister_script( 'jquery' );
		wp_deregister_script( 'jquery-migrate' );

		// wp_enqueue_script( 'underscore' );

		// Load the copy of jQuery that comes with WordPress  
		// The last parameter set to TRUE states that it should be loaded  
		// in the footer.  
		wp_enqueue_script( 'jquery', '/wp-includes/js/jquery/jquery.js', false, '1.11.0', true );
		wp_enqueue_script( 'jquery-migrate', '/wp-includes/js/jquery/jquery-migrate.min.js', false, '1.2.1', true );
	}

	function admin_enqueue_scripts( $hook ) 
	{
		// $this->enqueue_style( 'custom_wp_admin_css', 'admin/admin_style.css' );
	}

	protected function uri( $file )
	{
		$uri = THEME_URI;

		$path = $this->get_file_path( $file );

		return "$uri/$path";
	}

	protected function get_file_path( $file )
	{
		$type = pathinfo( $file, PATHINFO_EXTENSION );

		if ( !$type )
			return $file;

		$is_min = in_array( $file, $this->theme_minimized );

		$file = str_replace( ".{$type}", '', $file );

		if ( is_localhost() OR $this->is_localhost ) {
			if ( 'js' == $type ) {
				$abs  = THEME_INC_ABS .'/assets';

				$path = "src/.js-cache/{$file}.es6.{$type}";

				if ( file_exists( "$abs/$path" ) )
					return $path;
			}
		}

		if ( $is_min )
			$file .= $this->min;

		return "{$type}/{$file}.{$type}";
	}

	protected function get_file_version( $file )
	{
		$abs  = THEME_INC_ABS .'/assets';

		$path = $this->get_file_path( $file );

		if ( !file_exists( "$abs/$path" ) )
			return;

		return filemtime( "$abs/$path" );
	}

	protected function enqueue_script( $handle, $src = '', $deps = array(), $in_footer = false )
	{
		if ( $this->is_external( $src ) ) {
			$version = 1;
		} else {
			$version = $this->get_file_version( $src );
			$src     = $this->uri( $src );
		}

		wp_enqueue_script( $handle, $src, $deps, $version, $in_footer );
	}

	protected function enqueue_style( $handle, $src = '', $deps = array(), $media = 'all' )
	{
		if ( $this->is_external( $src ) ) {
			$version = 1;
		} else {
			$version = $this->get_file_version( $src );
			$src     = $this->uri( $src );
		}

		wp_enqueue_style( $handle, $src, $deps, $version, $media );
	}

	protected function is_external( $uri )
	{
		$external = array(
			'http',
			'//'
		);

		foreach ( $external as $start ) {
			if ( 0 === strpos( $uri, $start ) )
				return true;
		}

		return false;
	}

	public static function add_localize_script( $js_data = array(), $group = '' )
	{
		$data = self::$localize_script_data;

		$cache = wp_cache_get( $data['cache_key'] );

		if ( !$cache )
			$cache = array();

		if ( $group )
			$cache[ $group ] = $js_data;
		else
			$cache = wp_parse_args( $js_data, $cache );

		wp_cache_set( $data['cache_key'], $cache );
	}

	public static function print_extra_localize_script( $name, $data )
	{
		$key = __FUNCTION__ . '-'. rand( 0, 100 );
		
		wp_enqueue_script( $key, 'test.js' );
		wp_scripts()->localize( $key, $name, $data );
		wp_scripts()->print_extra_script( $key );
		wp_dequeue_script( $key );
	}

	public static function print_extra_inline_script( $path )
	{
		$key = __FUNCTION__ . '-'. rand( 0, 100 );

		$path = THEME_INC_ABS ."/assets/js/{$path}";

		ob_start();
		include $path;
		$script = ob_get_clean();

		wp_enqueue_script( $key, 'test.js' );
		wp_scripts()->add_inline_script( $key, $script );
		wp_scripts()->print_inline_script( $key );
		wp_dequeue_script( $key );
	}

	function do_theme_localize_script()
	{
		$data = self::$localize_script_data;

		$cache = wp_cache_get( $data['cache_key'] );

		if ( !$cache )
			return;

		wp_localize_script( $data['handler'], $data['name'], $cache );

		wp_cache_delete( $data['cache_key'] );
	}

	function add_defer_attribute( $tag, $handle )
	{
		// if ( isset( $_GET['test'] ) ) {
		// 	var_dump($handle);
		// }

		if ( in_array( $handle, $this->exclude_defer ) )
			return $tag;

		return str_replace( ' src', ' defer src', $tag );
	}

	protected function debug() 
	{
		if ( is_localhost() )
			$this->min = '';

		if ( isset( $_GET['debug'] ) ) {
			$this->min = '';
			$this->is_localhost = true;
		}
	}

	function __construct()
	{
		$this->debug();

		// if ( is_admin() ) {
		// 	add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );
		// 	return;
		// }

		// add_action( 'init', array( $this, 'enqueue_jquery_in_footer' ) );
		
		self::add_localize_script( array( 'ajaxUrl' => admin_url( 'admin-ajax.php' ) ) );
		add_action( 'wp_footer', array( $this, 'do_theme_localize_script' ), 19 );

		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ), 100 );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles'  ), 100 );


		add_action( 'wp_head',   array( $this, 'head_scripts' ) ); 
		add_action( 'wp_footer', array( $this, 'footer_scripts' ) );

		// add_action( 'theme_scripts_in_body', array( $this, 'scripts_in_body' ) );

		if ( $this->use_defer )
			add_filter( 'script_loader_tag', array( $this, 'add_defer_attribute' ), 10, 2 );
	}
}
new Theme_Scripts_Styles;

