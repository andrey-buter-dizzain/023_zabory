<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Different Template functions here
 */


// function theme_parse_data( $string ) {
// 	if ( !$string )
// 		return array();

// 	$string = trim( $string );
// 	$string = array_filter( explode( "\n", $string ) );

// 	$data = array();

// 	foreach ( $string as $value ) {
// 		$value = explode( '|', $value );

// 		$value = array_map( function( $val ) {
// 			return str_replace( array( "\r", '(', ')' ), '', $val );
// 		}, $value );

// 		$data[ $value[0] ] = $value[1];
// 	}

// 	return $data;
// }

/**
 * Function to return archive title and archive description
 *
 * @since Dizzain 1.0
 */
function get_theme_option_image( $img, $size, $class = '' ) {
	if ( absint( $img ) ) {
		$attr = $class ? compact( 'class' ) : array();

		return wp_get_attachment_image( $img, $size, false, $attr );
	}

	return "<img src=\"{$img}\" alt=\"\" class=\"attachment-{$size} {$class}\" />";
}

function get_theme_option_image_src( $key, $size = 'full' ) {
	if ( !$key )
		return;

	if ( absint( $key ) )
		return get_theme_image_src( $key, $size );

	if ( false === strpos( $key, '/' ) )
		return get_theme_option_image_src( get_theme_option( $key ), $size );

	return $key;
}

function get_theme_image_src( $attach_id, $size = 'thumbnail' ) {
	$src = wp_get_attachment_image_src( $attach_id, $size );

	return $src[0];
}





function get_theme_languages() {

	if ( function_exists('icl_get_languages') ) {
		$languages = icl_get_languages('skip_missing=0');
	} else {
		$languages = array(array(
				'id' => 1,
				'active' => 1,
				'native_name' => 'English',
				'missing' => 0,
				'translated_name' => 'English',
				'language_code' => 'en',
				'country_flag_url' => '',
				'url' => '',
		));
	}

	return $languages;
}







// add_filter( 'image_size_names_choose', 'my_image_sizes' );
// function my_image_sizes( $sizes ) {
// 	$sizes['project-in-content'] = __( 'Project Screen' );

// 	return $sizes;
// }



function get_the_post_thumbnail_src( $size, $post_id = null ) {
	$src = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), $size );

	if ( !$src )
		return;

	return $src[0];
}

function get_post_meta_image_src( $post_id, $meta_key, $size = '' ) {
	$image_id = get_post_meta( $post_id, $meta_key, true );

	if ( !absint( $image_id ) )
		return;

	$src = wp_get_attachment_image_src( $image_id, $size );

	if ( !$src )
		return;

	return $src[0];
}

function get_post_meta_image( $post_id, $meta_key, $size = '', $attr = array() ) {
	$image_id = get_post_meta( $post_id, $meta_key, true );

	if ( !absint( $image_id ) )
		return;

	return wp_get_attachment_image( $image_id, $size, false, $attr );
}

/**
 * Check localhost for debug
 */
function is_localhost() {
	if ( 'localhost' == $_SERVER['HTTP_HOST'] )
		return true;

	if ( false !== strpos ( $_SERVER['MYSQL_HOME'], 'xampp' ) )
		return true;

	return false;
}



/* =Theme Filters
-------------------------------------------------------------- */

/**
 * Sets the post excerpt length to 40 words.
 * Use global $excerpt_length to change excerpt length
 *
 * @param int number of words
 * @return int filtered number of words
 */
if ( !is_admin() )
	add_filter( 'excerpt_length', 'theme_name_excerpt_length' );
function theme_name_excerpt_length( $length ) {
	global $excerpt_length;

	$length = !empty($excerpt_length) ? absint($excerpt_length) : 37;

	// Unset global var
	if (!empty($excerpt_length))
		unset($GLOBALS['excerpt_length']);

	return $length;
}


/**
 * Replaces "[...]" (appended to automatically generated excerpts) with nothing.
 *
 * To override this in a child theme, remove the filter and add your own
 * function tied to the excerpt_more filter hook.
 */
add_filter( 'excerpt_more', 'theme_name_auto_excerpt_more' );
function theme_name_auto_excerpt_more( $more ) {
	return ' ...';
}

/**
 * Adds a pretty "Continue Reading" link to custom post excerpts.
 *
 * To override this link in a child theme, remove the filter and add your own
 * function tied to the get_the_excerpt filter hook.
 *
 * @param string $output post content or excerpt
 * @return string post excerpt with more link
 */
if ( !is_admin() )
	add_filter( 'wp_trim_excerpt', 'theme_name_custom_excerpt_more', 100 );
function theme_name_custom_excerpt_more( $output ) {
	return $output;
}


/**
 * Creates a nicely formatted and more specific title element text
 * for output in head of document, based on current view.
 *
 * @since nys 1.0
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string Filtered title.
 */
add_filter( 'wp_title', 'theme_name_wp_title', 10, 2 );
function theme_name_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_404() AND $page_title = get_theme_option( '404_title' ) )
		$title = "$page_title $sep ";

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'mushrif' ), max( $paged, $page ) );

	return $title;
}


/**
 * Set global var for excerpt lenght
 *
 * @param int $length of excerpt
 */
function set_excerpt_length($length) {
	$GLOBALS['excerpt_length'] = absint($length);
}

/**
 * Set global var for comment lenght
 *
 * @param int $length of content
 */
function set_comment_length($length) {
	$GLOBALS['comment_length'] = $length;
}

/**
 * Returns a "Continue Reading" link for excerpts
 */
function theme_name_continue_reading_link() {
	return ( 'episode' == get_post_type() ) ? '' : ' <a class="continue-reading" href="'. esc_url( get_permalink() ) . '">' . __( 'Continue Reading &raquo;', 'mushrif' ) .'</a>';
}

/**
 * Gallery is styled by theme css
 */
// add_filter( 'use_default_gallery_style', '__return_false' );


/**
 * Site © copyright
 *
 * if some years => ex., © 2012 - 2014
 * if one year => © 2012
 */
function wp_theme_copyright( $start_year ) {
	if ( absint( $start_year ) >= date( 'Y' ) )
		$period = date( 'Y' );
	else
		$period = $start_year . ' - ' . date( 'Y' );

	echo '&copy; ' . $period;
}


/**
 * Check if page is special
 */
function is_special_page( $keys ) {
	$result = false;

	if ( !is_page() )
		return $result;

	if ( !$keys )
		return $result;

	$keys = (array) $keys;

	foreach ( $keys as $key ) {
		$page_id = absint( get_theme_option( "{$key}_id" ) );

		if ( !$page_id )
			continue;

		$result = is_page( $page_id );

		if ( $result )
			break;
	}

	return $result;

	// return apply_filters( 'theme_is_special_page', $result, $keys, $page_id );
}

/**
 * Function to return archive title and archive description
 *
 * @since Dizzain 1.0
 */
function get_option_image( $size, $url, $img_id = null ) {
	if ( $img_id )
		return wp_get_attachment_image( $img_id, $size );
	elseif ( $url )
		return '<img src="'. $url .'" alt="" class="attachment-'. $size .'" />';
}

/**
 * Function to return archive title and archive description
 *
 * @since Dizzain 1.0
 */
function theme_archive_title() {
	if ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$title = single_tag_title( '', false );
	} elseif ( is_day() ) {
		$title = get_the_date();
	} elseif ( is_month() ) {
		$title = get_the_date( 'F Y' );
	} elseif ( is_year() ) {
		$title = get_the_date( 'Y' );
	} elseif( is_tax() ) {
		$title = get_queried_object()->name;
	} elseif ( is_post_type_archive() ) {
		$title = post_type_archive_title( '', false );
	}

	echo $title;
}


/**
 * Function to return soc buttons
 * $icons: linkedin, facebook, twitter, pinterest, google_plus, youtube, email
 *
 * @since Dizzain 1.0
 */
function social_block_2( $return = false, $class = '', $after = '' ) {
	$icons = get_theme_option( 'social-links' );

	if ( !$icons )
		return;

	foreach ( $icons as $value ) {
		$icon  = strtolower( $value['name'] );
		$link  = $value['href'];
		$title = $value['title'];

		$key = in_array( $icon, array( 'google', 'plusone', 'google+' ) ) ? 'google_plus' : $icon;

		$rel = ( 'google_plus' == $key ) ? 'publisher' : 'nofollow';

		$output .= "<a title=\"{$title}\" rel=\"{$rel}\" class=\"icon icon-{$icon} {$icon}\" target=\"_blank\" href=\"{$link}\">{$icon}</a>";
	}

	$output .= $after;

	if ( !$output )
		return;

	$output = "<div class=\"social-icons-block {$class}\">{$output}</div>";

	if ( $return )
		return $output;

	echo $output;
}

// function social_block( $icons = array(), $return = false, $class = '', $after = '' ) {
// 	if ( empty( $icons ) OR !is_array( $icons ) )
// 		return;

// 	foreach ( $icons as $icon ) {
// 		$key = in_array( $icon, array( 'google', 'plusone' ) ) ? 'google_plus' : $icon;

// 		$link = get_theme_option( $key );

// 		if ( !$link )
// 			continue;

// 		$link = $icon == 'email' ? 'mailto:'. $link : $link;

// 		$rel = ( 'google_plus' == $key ) ? 'publisher' : 'nofollow';

// 		$output .= "<a rel=\"{$rel}\" class=\"icon icon-{$icon} {$icon}\" target=\"_blank\" href=\"{$link}\">{$icon}</a>";
// 	}

// 	$output .= $after;

// 	if ( !$output )
// 		return;

// 	$output = "<div class=\"social-icons-block {$class}\">{$output}</div>";

// 	if ( $return )
// 		return $output;

// 	echo $output;
// }


/* =Remove gravtar.com
============================================================== */

add_filter( 'bp_core_fetch_avatar', 'bp_remove_gravatar', 1, 9 );
function bp_remove_gravatar ($image, $params, $item_id, $avatar_dir, $css_id, $html_width, $html_height, $avatar_folder_url, $avatar_folder_dir) {

	$default = '' ; /*get_stylesheet_directory_uri() .'/inc/assets/images/avatar_1x1.png';*/

	if( $image && strpos( $image, "gravatar.com" ) ) {
		return '<img src="' . $default . '" alt="avatar" class="avatar" ' . $html_width . $html_height . ' />';
	} else {
		return $image;
	}
}

add_filter( 'get_avatar', 'remove_gravatar', 1, 5 );
function remove_gravatar( $avatar, $id_or_email, $size, $default, $alt ) {
	// $default = get_stylesheet_directory_uri() .'/inc/assets/images/avatar_1x1.png';
	$default = '';
	return "<img alt='{$alt}' src='{$default}' class='avatar avatar-{$size} photo avatar-default' height='{$size}' width='{$size}' />";
}

add_filter( 'bp_get_signup_avatar', 'bp_remove_signup_gravatar', 1, 1 );
function bp_remove_signup_gravatar( $image ) {

	$default = '' ; /*get_stylesheet_directory_uri() .'/inc/assets/images/avatar_1x1.png';*/

	if( $image && strpos( $image, "gravatar.com" ) ) {
		return '<img src="' . $default . '" alt="avatar" class="avatar" width="60" height="60" />';
	} else {
		return $image;
	}
}


add_filter( 'post_class', 'theme_post_class', 10, 3 );
function theme_post_class( $classes, $class, $post_id ) {
	$post_type = get_post_type();

	$classes[] = "post-type-{$post_type}";

	return $classes;
}