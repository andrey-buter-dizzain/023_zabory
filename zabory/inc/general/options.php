<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Populate product meta boxes
 *
 * @package WordPress
 * @subpackage zabor
 */


add_filter( 'populate_theme_options', 'populate_theme_footer_options' );
function populate_theme_footer_options( $settings = array() ) {
	$id = __FUNCTION__;

	$settings['sections'][] = array(
		'title'   => __( 'General', 'zabor-admin' ),
		'id'      => $id
	);

	$settings['settings'][] = array(
		'id'      => 'price_link',
		'label'   => __( 'Прайс', 'zabor-admin' ),
		'type'    => 'upload',
		'wpmu'    => true,
		'section' => $id,
	);

	$settings['settings'][] = array(
		'id'      => 'contacts',
		'label'   => __( 'Контакты', 'zabor-admin' ),
		'type'    => 'textarea-simple',
		'rows'    => 3,
		'wpmu'    => true,
		'section' => $id,
	);

	$settings['settings'][] = array(
		'id'      => 'copyright',
		'label'   => __( 'Copyright &copy;', 'zabor-admin' ),
		'type'    => 'text',
		'wpmu'    => true,
		'section' => $id,
	);
	
	return $settings;
}
