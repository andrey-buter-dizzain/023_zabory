<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * Handle every member aspect
 */

add_filter( 'populate_theme_meta_boxes', 'populate_post_meta_boxes' );
function populate_post_meta_boxes( $meta_boxes = array() ) {
	$post_type = 'post';

	$prefix = "{$post_type}_";

	$meta_boxes[] = array(
		'id'       => "{$prefix}section",
		'title'    => __( 'Post meta', 'modulzabor-admin' ),
		'pages'    => array( $post_type ),
		'context'  => 'normal',
		'priority' => 'default',
		'fields'   => array(
			array(
				'label' => __( 'Popup', 'modulzabor-admin' ),
				'id'    => 'popup',
				'type'  => 'tab'
			),
			array(
				'label' => __( 'Тайтл', 'modulzabor-admin' ),
				'id'    => "{$prefix}popup_title",
				'type'  => 'text'
			),
			array(
				'label'     => __( 'Форма', 'modulzabor-admin' ),
				'id'        => "{$prefix}popup_form",
				'type'      => 'custom-post-type-select',
				'post_type' => 'wpcf7_contact_form',
			),
			array(
				'label'   => __( 'Параметры', 'modulzabor-admin' ),
				'id'      => "{$prefix}popup_data",
				'type'     => 'list-item',
				'settings' => array(
					array(
						'label' => __( 'Напротив тайтла', 'modulzabor-admin' ),
						'id'    => 'title-attr',
						'type'  => 'text',
					),
					array(
						'label' => __( 'Значения', 'modulzabor-admin' ),
						'id'    => 'text',
						'type'  => 'textarea-simple',
						'rows'  => 4,
					),
				),
			),
			array(
				'label' => __( 'Описание', 'modulzabor-admin' ),
				'id'    => "{$prefix}popup_desc",
				'type'  => 'textarea'
			),
			array(
				'label' => __( 'Картинки', 'modulzabor-admin' ),
				'id'    => "{$prefix}popup_gallery",
				'type'  => 'gallery'
			),
			array(
				'label' => __( 'Ananlytics', 'modulzabor-admin' ),
				'id'    => 'ananlytics',
				'type'  => 'tab'
			),
			array(
				'label' => __( 'GA Event Name', 'modulzabor-admin' ),
				'id'    => "{$prefix}ga_name",
				'type'  => 'text'
			),
		),
	);

	return $meta_boxes;
}
