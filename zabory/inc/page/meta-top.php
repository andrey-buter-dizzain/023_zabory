<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * Handle every member aspect
 */

add_filter( 'populate_theme_meta_boxes', 'populate_front_page_top_meta_boxes' );
function populate_front_page_top_meta_boxes( $meta_boxes = array() ) {
	$post_type = 'page';

	$prefix = "{$post_type}_top_";

	$meta_boxes[] = array(
		'id'       => "{$prefix}section",
		'title'    => __( '1. Верхний блок', 'zabor-admin' ),
		'pages'    => array( $post_type ),
		'context'  => 'normal',
		'priority' => 'default',
		'fields'   => array(
			array(
				'label' => __( 'Слайды', 'zabor-admin' ),
				'id'    => "{$prefix}gallery",
				'type'  => 'gallery'
			),
		),
		'only_on' => array(
			'function' => 'is_front_page'
		)
	);

	return $meta_boxes;
}