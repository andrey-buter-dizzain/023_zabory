<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * Handle every member aspect
 */

add_filter( 'populate_theme_meta_boxes', 'populate_front_page_advantages_meta_boxes' );
function populate_front_page_advantages_meta_boxes( $meta_boxes = array() ) {
	$post_type = 'page';

	$prefix = "{$post_type}_advantages_";

	$meta_boxes[] = array(
		'id'       => "{$prefix}section",
		'title'    => __( '1. Преимущества', 'jazeera-admin' ),
		'pages'    => array( $post_type ),
		'context'  => 'normal',
		'priority' => 'default',
		'fields'   => array(
			array(
				'label' => __( 'Тайтл', 'jazeera-admin' ),
				'id'    => "{$prefix}title",
				'type'  => 'text'
			),
			array(
				'label'   => __( 'Преимущества', 'jazeera-admin' ),
				'id'      => "{$prefix}data",
				'type'     => 'list-item',
				'settings' => array(
					array(
						'label' => __( 'Text', 'jazeera-admin' ),
						'id'    => 'text',
						'type'  => 'text',
					),
					array(
						'label' => __( 'Class', 'jazeera-admin' ),
						'id'    => 'class',
						'type'  => 'text',
					),
				),
			),
		),
		'only_on' => array(
			'function' => 'is_front_page'
		)
	);

	return $meta_boxes;
}