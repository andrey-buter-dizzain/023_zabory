<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * Handle every member aspect
 */

add_filter( 'populate_theme_meta_boxes', 'populate_front_page_sections_meta_boxes' );
function populate_front_page_sections_meta_boxes( $meta_boxes = array() ) {
	$post_type = 'page';

	$prefix = "{$post_type}_sections_";

	$meta_boxes[] = array(
		'id'       => "{$prefix}section",
		'title'    => __( '5. Заборные секции', 'zabor-admin' ),
		'pages'    => array( $post_type ),
		'context'  => 'normal',
		'priority' => 'default',
		'fields'   => array(
			array(
				'label' => __( 'Тайтл', 'zabor-admin' ),
				'id'    => "{$prefix}title",
				'type'  => 'text'
			),
		),
		'only_on' => array(
			'function' => 'is_front_page'
		)
	);

	return $meta_boxes;
}