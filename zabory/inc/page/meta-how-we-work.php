<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * Handle every member aspect
 */

add_filter( 'populate_theme_meta_boxes', 'populate_front_page_work_meta_boxes' );
function populate_front_page_work_meta_boxes( $meta_boxes = array() ) {
	$post_type = 'page';

	$prefix = "{$post_type}_work_";

	$meta_boxes[] = array(
		'id'       => "{$prefix}section",
		'title'    => __( '6. Как мы работаем', 'zabor-admin' ),
		'pages'    => array( $post_type ),
		'context'  => 'normal',
		'priority' => 'default',
		'fields'   => array(
			array(
				'label' => __( 'Тайтл', 'zabor-admin' ),
				'id'    => "{$prefix}title",
				'type'  => 'text'
			),
			array(
				'label'   => __( 'Пункты', 'zabor-admin' ),
				'id'      => "{$prefix}data",
				'type'     => 'list-item',
				'settings' => array(
					array(
						'label' => __( 'Text', 'zabor-admin' ),
						'id'    => 'text',
						'type'  => 'textarea',
						'rows'  => 4,
 					),
					array(
						'label' => __( 'Class', 'zabor-admin' ),
						'id'    => 'class',
						'type'  => 'text',
					),
				),
			),
			array(
				'label' => __( 'Текст кнопки для звонка', 'jazeera-admin' ),
				'id'    => "{$prefix}button",
				'type'  => 'text'
			),
			array(
				'label'     => __( 'Форма', 'jazeera-admin' ),
				'id'        => "{$prefix}form",
				'type'      => 'custom-post-type-select',
				'post_type' => 'wpcf7_contact_form',
			),
		),
		'only_on' => array(
			'function' => 'is_front_page'
		)
	);

	return $meta_boxes;
}