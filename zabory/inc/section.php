<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Page module
 */

class Inc_Section_Post extends Inc_Load_Section
{
	public static $section = 'section';

	protected static $include_path;

	protected static $uri_path;

	public static $partials = array(
		'model',
		'meta',
		'public',
	);
}
Inc_Section_Post::load();