<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Page module
 */

class Inc_Page extends Inc_Load_Section
{
	public static $section = 'page';

	protected static $include_path;

	protected static $uri_path;

	public static $partials = array(
		// 'meta-advantages',
		'meta-top',
		'meta-advantages-2',
		'meta-description',
		// 'meta-description-2',
		'meta-products',
		'meta-products-2',
		'meta-sections',
		'meta-how-we-work',
	);
}
Inc_Page::load();