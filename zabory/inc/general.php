<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * General Sue Mathys theme handler
 */

class Inc_General extends Inc_Load_Section
{
	public static $section = 'general';

	protected static $include_path;

	protected static $uri_path;

	public static $partials = array(
		'options',
		'custom',
		'public',
		'script-style',
	);
}
Inc_General::load();