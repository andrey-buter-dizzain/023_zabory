<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * Handle every member aspect
 */

add_filter( 'populate_theme_meta_boxes', 'populate_profile_meta_boxes' );
function populate_profile_meta_boxes( $meta_boxes = array() ) {
	$post_type = 'profile';

	$prefix = "{$post_type}_slider_";

	$meta_boxes[] = array(
		'id'       => "{$prefix}meta",
		'title'    => __( 'Slider Data', 'nrec-admin' ),
		'pages'    => array( $post_type ),
		'context'  => 'normal',
		'priority' => 'default',
		'fields'   => array(
			array(
				'label'   => __( 'Position', 'nrec-admin' ),
				'id'      => "{$prefix}position",
				'type'    => 'text',
			),
		),
	);

	$prefix = "{$post_type}_letters_";

	$meta_boxes[] = array(
		'id'       => "{$prefix}meta",
		'title'    => __( 'Letters Page Data', 'nrec-admin' ),
		'pages'    => array( $post_type ),
		'context'  => 'normal',
		'priority' => 'default',
		'fields'   => array(
			array(
				'label'   => __( 'Block title', 'nrec-admin' ),
				'id'      => "{$prefix}title",
				'type'    => 'text',
			),
			array(
				'label'   => __( 'Quote', 'nrec-admin' ),
				'id'      => "{$prefix}quote",
				'type'    => 'textarea-simple',
				'rows'    => 4
			),
		),
	);

	return $meta_boxes;
}