<?php
/**
 * Register Section post type
 */

register_post_type( 'section',
	array(
		'labels'        => array(
			'name'               => __( 'Sections', 'zabor-admin' ),
			'singular_name'      => __( 'Section', 'zabor-admin' ),
			'add_new'            => __( 'Add Section', 'zabor-admin' ),
			'add_new_item'       => __( 'Add New Section', 'zabor-admin' ),
			'edit'               => __( 'Edit', 'zabor-admin' ),
			'edit_item'          => __( 'Edit Section', 'zabor-admin' ),
			'new_item'           => __( 'New Section', 'zabor-admin' ),
			'view'               => __( 'View Sections', 'zabor-admin' ),
			'view_item'          => __( 'View Section', 'zabor-admin' ),
			'search_items'       => __( 'Search Sections', 'zabor-admin' ),
			'not_found'          => __( 'Not found', 'zabor-admin' ),
			'not_found_in_trash' => __( 'No Sections found in Trash', 'zabor-admin' ),
		),
		'public'        => true,
		'menu_position' => 20,
		'hierarchical'  => false,
		'has_archive'   => false,
		'query_var'     => true,
		'supports'      => array( 
			'title', 
			// 'editor', 
			'thumbnail', 
			// 'custom-fields',
			// 'excerpt',
		),
		'rewrite'       => false,
		'can_export'    => true,
		'publicly_queryable'  => false, // ???????????
		'exclude_from_search' => true
	)
);