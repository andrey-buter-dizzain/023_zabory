<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

function get_simple_site_url( $url ) {
	$url = str_replace( 'http://', '', $url );
	$url = str_replace( 'https://', '', $url );

	if ( '/' == substr( $url, -1 ) )
		$url = substr( $url, 0, strlen( $url ) - 1 );

	// if ( 'www.' == substr( $url, 0, 4 ) )
	// 	$url = substr( $url, 4 );

	return $url;
}


add_filter( 'wpbs_sticky_tmpl_args', 'set_wpbs_sticky_tmpl_args' );
function set_wpbs_sticky_tmpl_args( $args = array() ) {
	$img_id = get_the_post_meta( 'project_image_id' );

	if ( !$img_id )
		$img_id = get_the_post_meta( 'project_image_mobile_id' );

	if ( !$img_id )
		return $args;

	$src = wp_get_attachment_image_src( $img_id, 'thumbnail' );

	if ( !$src )
		return $args;

	$args['image'] = $src[0];

	return $args;
}