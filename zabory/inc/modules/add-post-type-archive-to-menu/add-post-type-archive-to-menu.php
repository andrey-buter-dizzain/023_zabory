<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 


add_action( 'admin_head-nav-menus.php', 'module_add_metabox_menu_posttype_archive' );

function module_add_metabox_menu_posttype_archive() {
	add_meta_box(
		'module-metabox-nav-menu-posttype', 
		__( 'Custom Post Type Archives', 'module-post-type-archive-to-menu' ), 
		'module_metabox_menu_posttype_archive', 
		'nav-menus', 
		'side', 
		'default'
	);
}

function module_metabox_menu_posttype_archive() {
	$post_types = get_post_types( array(
		'show_in_nav_menus' => true, 
		'has_archive'       => true
	), 'object' );

	if ( !$post_types )
		return;

	$items = array();
	$loop_index = 999999;

	foreach ( $post_types as $post_type ) {
		$item = new stdClass();
		$loop_index++;

		$item->object_id  = $loop_index;
		$item->db_id      = 0;
		$item->object     = 'post_type_' . $post_type->query_var;
		$item->menu_item_parent = 0;
		$item->type       = 'custom';
		$item->title      = $post_type->labels->name;
		$item->url        = get_post_type_archive_link( $post_type->query_var );
		$item->target     = '';
		$item->attr_title = '';
		$item->classes    = array();
		$item->xfn        = '';

		$items[] = $item;
	}

	$walker = new Walker_Nav_Menu_Checklist( array() );
?>
	<div id="posttype-archive" class="posttypediv">
		<div id="tabs-panel-posttype-archive" class="tabs-panel tabs-panel-active">
			<ul id="posttype-archive-checklist" class="categorychecklist form-no-clear">
		 		<?php 
		 			echo walk_nav_menu_tree( 
		 				array_map( 'wp_setup_nav_menu_item', $items ), 
		 				0, 
		 				(object) array( 'walker' => $walker ) 
		 			); 
		 		?>
			</ul>
		</div>
	</div>

	<p class="button-controls">
		<span class="add-to-menu">
			<input type="submit" <?php disabled( 1, 0, true ) ?> class="button-secondary submit-add-to-menu right" value="<?php _e( 'Add to Menu', 'module-post-type-archive-to-menu' ) ?>" name="add-posttype-archive-menu-item" id="submit-posttype-archive" />
			<span class="spinner"></span>
		</span>
	</p>
<?php 
}