<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

if ( !is_admin() )
	return;

if ( !defined( 'USER_META_ABS' ) )
	define( 'USER_META_ABS', dirname( __FILE__ ) );

if ( !defined( 'USER_META_FOLDER' ) )
	define( 'USER_META_FOLDER', basename( USER_META_ABS ) );

if ( !defined( 'USER_META_PATH' ) )
	define( 'USER_META_PATH', THEME_MODULE_PATH .'/'. USER_META_FOLDER );


/**
 * Add User meta fields
 */
class Module_User_Meta
{
	protected $user_meta = array();

	function __construct()
	{
		add_action( 'show_user_profile', array( $this, 'show_user_fields' ) );
		add_action( 'edit_user_profile', array( $this, 'show_user_fields' ) );
		add_action( 'user_new_form', array( $this, 'show_user_fields' ) );
		 
		add_action( 'profile_update', array( $this, 'save_meta' ) );
		add_action( 'user_register', array( $this, 'save_meta' ) );

		add_action( 'init', array( $this, 'load_fields' ) );
	}

	function load_fields()
	{
		$this->user_meta = apply_filters( 'module_user_meta_fileds', $this->user_meta );
	}

	function show_user_fields( $user )
	{
		if ( empty( $this->user_meta ) )
			return;

		if ( !is_array( $this->user_meta ) )
			return;

		$path = USER_META_PATH .'/tmpl';

		get_theme_part( $path, 'admin-meta-tmpl', array(
			'return' => false,
			'data'   => array(
				'user_meta' => $this->user_meta,
				'user_id'   => $user->ID
			)
		) );
	}

	protected function is_user_meta_active()
	{
		if ( !current_user_can( 'edit_user' ) )
			return false;

		if ( empty( $this->user_meta ) )
			return false;

		return is_array( $this->user_meta );
	}

	function save_meta( $user_id )
	{
		if ( !$this->is_user_meta_active() )
			return;

		foreach ( $this->user_meta as $group ) {
			if ( !$group['fields'] )
				continue;

			foreach ( $group['fields'] as $field ) {
				$key = $field['id'];

				$value = sanitize_text_field( $_POST[ $key ] );

				if ( in_array( $value, array( '', false ) ) )
					delete_user_meta( $user_id, $key );
				else
					update_user_meta( $user_id, $key, $value );
			}
		}
	}
}
new Module_User_Meta;