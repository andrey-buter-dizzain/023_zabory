<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

add_filter( 'module_user_meta_fileds', 'module_user_meta_seo_fileds' );
function module_user_meta_seo_fileds( $user_meta = array() )
{
	$user_meta[] = array(
		'title'  => __( 'Gallery Guide SEO', 'module-user-meta' ),
		'fields' => array(
			array(
				'label' => __( 'SEO Title' ,'module-user-meta' ),
				'id'    => 'seo_title',
				'type'  => 'text',
				'desc'  => ''
			),
			array(
				'label' => __( 'Meta Description' ,'module-user-meta' ),
				'id'    => 'seo_desc',
				'type'  => 'textarea',
				'desc'  => ''
			),
		),
	);

	return $user_meta;
}