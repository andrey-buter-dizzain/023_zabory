<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$data = get_theme_part_data();

$user_id = $data['user_id'];

?>
<?php foreach ( (array) $data['user_meta'] as $group ): ?>
	<?php if ( $group['title'] ): ?>
		<h3><?php echo $group['title'] ?></h3>
	<?php endif ?>
	
	<table class="form-table">
		<?php foreach ( (array) $group['fields'] as $field ): ?>
			<tr>
				<th>
					<label for="<?php echo $field['id'] ?>">
						<?php echo $field['label'] ?>
						
						<?php if ( $field['title_desc'] ): ?>
							<br /><span class="description"><?php echo $field['title_desc'] ?></span>
						<?php endif ?>
					</label>
				</th>
				<td>
					<?php 
						$field['value'] = get_the_author_meta( $field['id'], $user_id );

						if ( empty( $field['value'] ) )
							$field['value'] = $field['default'];

						get_theme_part( 
							USER_META_PATH .'/fields', 
							$field['type'], 
							array(
								'data' => compact( 'field', 'user_id' ) 
							) 
						);
					?>

					<?php if ( $field['desc'] ): ?>
						<br /><span class="description"><?php echo $field['desc'] ?></span>
					<?php endif ?>	
				</td>
			</tr>
		<?php endforeach ?>
	</table>
<?php endforeach;