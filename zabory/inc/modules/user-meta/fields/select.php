<?php 
/**
 * Select filed template
 *
 * Simple term meta
 */

$data = get_theme_part_data();

extract( $data );

?>
<select name="<?php echo $field['id'] ?>" id="<?php echo $field['id'] ?>">
	<?php foreach ( $field['choices'] as $key => $option ): ?>
		<option value="<?php echo $key ?>" <?php selected( $field['value'], $key ) ?>><?php echo $option ?></option>
	<?php endforeach ?>
</select>