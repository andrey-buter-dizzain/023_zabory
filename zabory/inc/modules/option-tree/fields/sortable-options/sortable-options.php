<?php
/**
 * Options tree theme adapter
 *
 * Each theme option is retrieved via get_theme_option filter
 */

/**
* Add sort ability for all options tree checkboxes
*/
class Theme_OptionTree_Sortable 
{
	protected $options = array();

	function __construct() 
	{
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_script' ) );
		add_filter( 'ot_display_by_type', array( $this, 'store_sortable_values' ) );

		add_action( 'admin_footer', array( $this, 'localize_sortable_fields' ) );
	}

	function store_sortable_values( $args ) 
	{
		$value = $args['field_value'];
		$id    = $args['field_id'];

		if ( false !== strpos( $args['field_class'], 'theme-sortable-option' ) )
			$this->options[ $id ] = is_array( $value ) ? array_values( $value ) : $value;

		return $args;
	}

	/**
	 * Additional OptionTree Scripts
	 */
	function enqueue_script() 
	{
		wp_enqueue_script( 'theme_sortable_option', THEME_MODULE_URI .'/option-tree/fields/sortable-options/assets/sortable-options.js', array( 'jquery-ui-sortable' ), '1.0' , true );
	}

	function localize_sortable_fields() 
	{
		if ( !$this->options )
			return;

		wp_localize_script( 'theme_sortable_option', 'screenSortableOptions', $this->options );
	}
}

if ( is_admin() )
	new Theme_OptionTree_Sortable;