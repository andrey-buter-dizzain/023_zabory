<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * Custom Post Type Select option type.
 *
 * See @ot_display_by_type to see the full list of available arguments.
 *
 * @param     array     An array of arguments.
 * @return    string
 *
 * @access    public
 * @since     2.0
 */
if ( ! function_exists( 'ot_type_custom_post_type_select_2' ) ) {
	
	function ot_type_custom_post_type_select_2( $args = array() ) {

		/* turns arguments array into variables */
		extract( $args );
		
		/* verify a description */
		$has_desc = $field_desc ? true : false;
		
		/* format setting outer wrapper */
		echo '<div class="format-setting type-custom-post-type-select ' . ( $has_desc ? 'has-desc' : 'no-desc' ) . '">';
			
			/* description */
			echo $has_desc ? '<div class="description">' . htmlspecialchars_decode( $field_desc ) . '</div>' : '';
			
			/* format setting inner wrapper */
			echo '<div class="format-setting-inner">';
				
				/* build category */
				echo '<select name="' . esc_attr( $field_name ) . '" id="' . esc_attr( $field_id ) . '" class="option-tree-ui-select ' . $field_class . '">';
				
				/* setup the post types */
				$post_type = isset( $field_post_type ) ? explode( ',', $field_post_type ) : array( 'post' );
				
				/* query posts array */
				$my_posts = get_posts( apply_filters( 'ot_type_custom_post_type_select_query', array( 'post_type' => $post_type, 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC', 'post_status' => 'any' ), $field_id ) );
				
				/* has posts */
				if ( is_array( $my_posts ) && ! empty( $my_posts ) ) {
					echo '<option value="">-- ' . __( 'Choose One', 'option-tree' ) . ' --</option>';
					foreach( $my_posts as $my_post ) {
						$post_title = '' != $my_post->post_title ? $my_post->post_title : 'Untitled';
						echo '<option value="' . esc_attr( $my_post->ID ) . '"' . selected( $field_value, $my_post->ID, false ) . '>' . $post_title . ' ('. $my_post->ID .')</option>';
					}
				} else {
					echo '<option value="">' . __( 'No Posts Found', 'option-tree' ) . '</option>';
				}
				
				echo '</select>';
				
			echo '</div>';

		echo '</div>';
		
	}
	
}