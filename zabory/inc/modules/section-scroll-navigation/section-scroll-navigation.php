<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * 
 */
class Module_Scroll_Section_Navigation
{
	protected $navigation = 'primary';

	protected $sections = array();

	protected $section_prefix = 'section-';

	protected $index = 0;

	protected $hook_string = 'xxxxx';

	protected static $instance;

	protected function __construct()
	{
		add_filter( 'nav_menu_link_attributes', array( $this, 'nav_menu_link_attributes' ), 10, 4 );

		add_filter( 'wp_nav_menu_items', array( $this, 'wp_nav_menu_items' ), 10, 2 );
	}

	function wp_nav_menu_items( $items, $args )
	{
		if ( $this->navigation != $args->theme_location )
			return $items;
		
		remove_filter( 'wp_nav_menu_items', array( $this, 'wp_nav_menu_items' ), 10, 2 );

		return str_replace( $this->hook_string, '', $items );
	}

	function nav_menu_link_attributes( $atts, $item, $menu, $depth )
	{
		if ( $this->navigation != $menu->theme_location )
			return $atts;

		$prefix = "#{$this->section_prefix}";

		if ( 0 !== strpos( $item->url, $prefix ) )
			return $atts;

		// т.к. делается проверка значения через empty()
		// в Walker_Nav_Menu::start_el(),
		// то чтоб не рпопал 0, приписываем ему постфиком любую нестандартную строку
		// а потом удалим ее
		$postfix_hook = 0 == $this->index ? $this->hook_string : '';

		$atts['data-scroll-nav'] = $this->index . $postfix_hook;

		$this->sections[ $this->index ] = str_replace( $prefix, '', $item->url );

		++$this->index;

		return $atts;
	}

	public function get_index( $section )
	{
		return array_search( $section, $this->sections );
	}

	public static function inst()
	{
		if ( !self::$instance )
			self::$instance = new self();

		return self::$instance;
	}
}
Module_Scroll_Section_Navigation::inst();

function module_scroll_data_index( $section ) {
	$index = Module_Scroll_Section_Navigation::inst()->get_index( $section );

	if ( false === $index )
		return;

	echo "data-scroll-index=\"{$index}\"";
}