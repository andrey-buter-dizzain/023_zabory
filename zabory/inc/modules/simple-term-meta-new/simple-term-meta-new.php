<?php 

/**
 * Simple_Term_Meta_New
 */
class Simple_Term_Meta_New
{
	protected $tax_meta_boxes = array();

	protected $default_field = array(
		'label'    => '',
		'id'       => '',
		'type'     => '',
		'choices'  => array(),
		'desc'     => '',
		'rows'     => 5,
		'abs_path' => '',
		'data'     => null
	);

	protected $abs_path;

	function __construct()
	{
		global $pagenow;

		if ( !in_array( $pagenow, array( 'edit-tags.php', 'term.php' ) ) )
			return;

		add_action( 'init', array( $this, 'load_term_meta' ) );

		// allow HTML in category description
		remove_filter( 'pre_term_description', 'wp_filter_kses' );
		remove_filter( 'term_description', 'wp_kses_data' );

		$this->abs_path = THEME_MODULE_ABS .'/'. basename( __DIR__ ) .'/fields';
	}

	function load_term_meta()
	{
		$tax_meta_boxes = apply_filters( 'simple_term_meta_fields_new', array(), 100 );

		if ( !$tax_meta_boxes )
			return;

		add_action( "edited_term", array( $this, 'save_term_meta' ), 10, 3 );		
		add_action( "create_term", array( $this, 'save_term_meta' ), 10, 3 );

		$this->set_tax_meta_boxes( $tax_meta_boxes );

		foreach ( $this->get_tax_meta_boxes() as $taxonomy => $tax_meta ) {
			add_action( "{$taxonomy}_add_form_fields",  array( $this, 'add_new_term_page_meta_fields' ) );
			add_action( "{$taxonomy}_edit_form_fields", array( $this, 'edit_term_page_meta_fields' ), 10, 2 );
		}
	}

	/**
	 * Отображение полей на странице add new term
	 */
	function add_new_term_page_meta_fields( $taxonomy )
	{
		$tax_meta_box = $this->get_tax_meta_boxes( $taxonomy );

		foreach ( $tax_meta_box['fields'] as $field ) {
			$field = shortcode_atts( $this->default_field , $field );

			extract( $field );

			if ( !$id )
				continue;

			if ( !$type )
				continue;

			?>
				<div class="form-field">
					<label for="<?php echo $id ?>"><?php echo $label ?></label>

					<?php $this->load_meta_field( $taxonomy, $field ) ?>

					<?php if ( !empty( $desc ) ): ?>
						<p class="description"><?php echo $desc ?></p>
					<?php endif ?>

				</div>
			<?php 	
		}
	}

	/**
	 * Отображение полей на странице add new term
	 */
	function edit_term_page_meta_fields( $term, $taxonomy )
	{
		$tax_meta_box = $this->get_tax_meta_boxes( $taxonomy );

		foreach ( $tax_meta_box['fields'] as $field ) {
			$field = shortcode_atts( $this->default_field , $field );

			extract( $field );

			if ( !$id )
				continue;

			if ( !$type )
				continue;

			?>
				<tr class="form-field">
					<th scope="row" valign="top">
						<label for="<?php echo $id ?>"><?php echo $label ?></label>
					</th>
					<td>
						<?php $this->load_meta_field( $taxonomy, $field, get_term_meta( $term->term_id, $field['id'], true ) ) ?>

						<?php if ( !empty( $desc ) ): ?>
							<p class="description"><?php echo $desc ?></p>	
						<?php endif ?>
					</td>
				</tr>
			<?php 	
		}
	}

	/**
	 * Load meta field
	 */
	protected function load_meta_field( $taxonomy, $field, $value = '' )
	{
		$path = empty( $field['abs_path'] ) ? $this->abs_path : $field['abs_path'];

		$field_path = "{$path}/{$field['type']}.php";

		if ( !file_exists( $field_path ) ) {
			echo 'Field not isset';
			return;
		}

		extract( $field );

		$name = "{$taxonomy}_meta[{$id}]";

		require( $field_path );
	}

	/**
	 * Setter - Создаем новый формат массива taxes_meta, где ключ к элементу = taxonomy
	 */
	protected function set_tax_meta_boxes( $tax_meta_boxes )
	{
		$taxes_meta_sorted = array();

		foreach ( $tax_meta_boxes as $tax_meta_box ) {
			$taxonomy = $tax_meta_box['taxonomy'];

			$taxes_meta_sorted[$taxonomy] = $tax_meta_box;
		}

		$this->tax_meta_boxes = $taxes_meta_sorted;
	}

	/**
	 * Getter taxes_meta array
	 */
	protected function get_tax_meta_boxes( $taxonomy = '' )
	{
		if ( $taxonomy )
			return $this->tax_meta_boxes[ $taxonomy ];

		return $this->tax_meta_boxes;
	}

	/**
	 * Save term meta
	 */
	function save_term_meta( $term_id, $tt_id, $taxonomy ) 
	{
		$new_data = $this->get_term_request_data( $taxonomy );

		foreach ( $new_data as $key => $value ) {
			if ( '' === $value )
				delete_term_meta( $term_id, $key );
			else 
				update_term_meta( $term_id, $key, $value );
		}
	}

	protected function get_term_request_data( $taxonomy )
	{
		$meta_key = "{$taxonomy}_meta";

		$new_data = array();

		if ( !isset( $_POST[ $meta_key ] ) )
			return $new_data;

		$tax_meta = $this->get_tax_meta_boxes( $taxonomy );

		if ( !$tax_meta )
			return $new_data;

		$meta_data = $_POST[ $meta_key ];


		foreach ( $tax_meta['fields'] as $field ) {
			$id = $field['id'];

			if ( !isset( $meta_data[$id] ) )
				continue;

			$new_data[$id] = $meta_data[$id];
		}

		return $new_data;
	}
}

if ( is_admin() ) 
	new Simple_Term_Meta_New;

