<?php 

// Load option tree plugin in theme

if ( is_admin() ) {

	// Hide the settings & documentation pages.
	add_filter( 'ot_show_pages', '__return_false' );

	// Enable Theme mode
	add_filter( 'ot_theme_mode', '__return_true' );

	/*
	 * Allow using lists for options with title only fields
	 */
	add_filter( 'ot_list_item_settings', 'remove_default_slider_filelds', 12 );
	function remove_default_slider_filelds() {
		return array();
	}

	// @since OT v2.4.2 
 // WARNING!!! 
 // Added filter ot_override_forced_textarea_simple to allow the Textarea option type to be moved in the DOM and not replaced with the Textarea Simple option type in meta boxes and list items.
	add_filter( 'ot_override_forced_textarea_simple', '__return_true' );
}

// Include OptionTree.
include_once( get_template_directory() .'/option-tree/ot-loader.php' );