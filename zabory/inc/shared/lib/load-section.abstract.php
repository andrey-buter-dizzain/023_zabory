<?php 

/**
 * 
 */
class Inc_Load_Section
{
	/**
	 * Module name
	 */
	public static $section;

	/**
	 * Include path
	 */
	protected static $include_path;

	/**
	 * Folder of section
	 */
	protected static $folder = 'inc';

	/**
	 * Uri path
	 */
	protected static $uri_path;

	/**
	 * List section partials
	 */
	public static $partials = array();

	/**
	 * Pre load function
	 */
	public static function load()
	{
		static::pre_load();

		if ( ! static::$section )
			return;

		static::$include_path = get_template_directory() . '/'. static::$folder .'/'. static::$section .'/';
		
		static::include_partials();
		static::on_load();
	}

	/**
	 * Preload function
	 */
	public static function pre_load() {}

	/**
	 * Onload function
	 */
	public static function on_load() {}

	/**
	 * Load product model partials
	 * @return void
	 */
	protected static function include_partials( $partials = array() ) 
	{
		$partials = !empty( $partials ) ? $partials : static::$partials;

		foreach ( $partials as $partial ) {
			$partial = static::get_partial( $partial );

			require_once( static::$include_path . $partial .'.php' );
		}
	}

	/**
	 * Get partial function
	 */
	protected static function get_partial( $partial = '' )
	{
		return $partial;
	}

	// function get_plugins($section_folder = '') {
	// 	$wp_plugins = array();

	// 	$section_root = WP_PLUGIN_DIR;

	// 	if ( !empty($section_folder) )
	// 		$section_root .= $section_folder;

	// 	$plugin_files = array();

	// 	// Files in wp-content/plugins directory
	// 	$plugins_dir = @opendir( $section_root );

	// 	if ( $plugins_dir ) {
	// 		while( ($file = readdir( $plugins_dir ) ) !== false ) {
	// 			if ( substr( $file, 0, 1 ) == '.' )
	// 				continue;

	// 			if ( is_dir( "$section_root/$file" ) ) {
	// 				$plugins_subdir = @ opendir( $plugin_root.'/'.$file );

	// 				if ( $plugins_subdir ) {
	// 					while (($subfile = readdir( $plugins_subdir ) ) !== false ) {
	// 						if ( substr($subfile, 0, 1) == '.' )
	// 							continue;
	// 						if ( substr($subfile, -4) == '.php' )
	// 							$plugin_files[] = "$file/$subfile";
	// 					}
	// 					closedir( $plugins_subdir );
	// 				}
	// 			} else {
	// 				if ( substr($file, -4) == '.php' )
	// 					$plugin_files[] = $file;
	// 			}
	// 		}
	// 		closedir( $plugins_dir );
	// 	}

	// 	if ( empty($plugin_files) )
	// 		return $wp_plugins;

	// 	foreach ( $plugin_files as $plugin_file ) {
	// 		if ( !is_readable( "$plugin_root/$plugin_file" ) )
	// 			continue;

	// 		$plugin_data = get_plugin_data( "$plugin_root/$plugin_file", false, false ); //Do not apply markup/translate as it'll be cached.

	// 		if ( empty ( $plugin_data['Name'] ) )
	// 			continue;

	// 		$wp_plugins[plugin_basename( $plugin_file )] = $plugin_data;
	// 	}

	// 	uasort( $wp_plugins, '_sort_uname_callback' );

	// 	$cache_plugins[ $plugin_folder ] = $wp_plugins;
	// 	wp_cache_set('plugins', $cache_plugins, 'plugins');

	// 	return $wp_plugins;
	// }

	// function check_folder_name( $folder )
	// {
	// 	if ( substr( $folder, 0, 1 ) == '.' )
	// 		return false;

	// 	if ( substr( $folder, -4 ) == '.php' )
	// 		return false;

	// 	if ( substr( $folder, 0, 1 ) == '_' )
	// 		return false;

	// 	if ( preg_match(  ) )
	// 		return false;
	// }
}