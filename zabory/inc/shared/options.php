<?php
/**
 * Options tree theme adapter
 *
 * Each theme option is retrieved via get_theme_option filter
 */

/**
 * Get options tree option
 * Options tree class is loaded for admin only
 *
 * Helper function to return the option value.
 * If no value has been saved, it returns $default.
 *
 * @param     string    The option ID.
 * @param     string    The default option value.
 * @return    mixed
 *
 */
if ( ! function_exists( 'ot_get_option' ) ) {
	function ot_get_option( $option_id, $default = '' ) {

		/* get the saved options */ 
		$options = get_option( 'option_tree' );

		/* look for the saved value */
		if ( isset( $options[ $option_id ] ) && !empty( $options[ $option_id ] ) ) {
			return $options[ $option_id ];
		}

		return $default;
	}
}


/* =Add Theme Options support
 * currently OptionsTree is used
-------------------------------------------------------------- */

/**
 * Filter option extraction for our theme
 * @param  mixed $default Option default value
 * @param  string $key    Options key
 * @return mixed          Option value
 */
add_filter( 'theme_option', 'ot_theme_option_adapter', 10, 2 );
function ot_theme_option_adapter( $default, $key ) {
	return ot_get_option( $key, $default );
}

/**
* Options tree backend is for admin purposes only
*/
if ( !is_admin() ) 
	return;


/**
 * Update theme options for current theme
 * providing public filter for options fields
 *
 * @return    void
 * @since     2.0
 */
add_action( 'admin_init', 'theme_custom_theme_options', 1 );
function theme_custom_theme_options() {
	$saved_settings = get_option( 'option_tree_settings', array() );

	$custom_settings = array(
		'sections' => array(),
		'settings' => array()
	);

	$custom_settings = apply_filters( 'populate_theme_options', $custom_settings );

	if ( defined( 'THEME_LANG_PREFIX' ) AND '' != THEME_LANG_PREFIX )
		$custom_settings = theme_set_wpmu_options( $custom_settings );

	if ( $saved_settings === $custom_settings ) 
		return;

	update_option( 'option_tree_settings', $custom_settings );

	wp_redirect( current_url() );
	exit;
}

function theme_set_wpmu_options( $settings ) {
	$settings = array_map( function( $section ) {
		$langs = get_theme_languages();
		$new   = array();

		foreach ( $section as $field ) {
			if ( !( isset( $field['wpmu'] ) AND $field['wpmu'] ) ) {
				$new[] = $field;
				continue;
			}

			$original = $field;

			foreach ( $langs as $lang )
				$new[] = theme_wpmu_modificate_option_field( $field, $lang );
		}

		return $new;
	}, $settings );

	return $settings;
}

function theme_wpmu_modificate_option_field( $field, $lang ) {
	$postfix = "_{$lang['language_code']}";

	$postfix_default = '_'. THEME_LANG_DEFAULT;

	if ( $postfix_default == $postfix )
		$postfix = '';

	$field['id']   .= $postfix;
	$field['label'] = "{$field['label']} <span style=\"color:red;\">({$lang['translated_name']})</span>";

	if ( '_all' != THEME_LANG_PREFIX AND !absint( $lang['active'] ) ) {
		$field['label'] = '';
		$field['type']  = 'hidden';
	}

	return $field;
}
