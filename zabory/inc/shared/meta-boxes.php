<?php
/**
 * Register meta boxes
 * Including the meta boxes class
 * Meta boxes are added using options tree plugin
 */

/**
 * This module is backend only and for special pages
 */

if ( !is_admin() )
	return;


/**
 * Register meta boxes
 *
 * @return void
 */
add_action( 'admin_init', 'theme_register_meta_boxes' );
function theme_register_meta_boxes() {

	$meta_boxes = apply_filters( 'populate_theme_meta_boxes', array() );

	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if ( !function_exists( 'ot_register_meta_box' ) )
		return;

	foreach ( $meta_boxes as $meta_box )
	{
		if ( isset( $meta_box['only_on'] ) && !theme_meta_box_maybe_include( $meta_box['only_on'] ) )
			continue;

		// new RW_Meta_Box( $meta_box );
		ot_register_meta_box( $meta_box );
	}
	// unset global variable
	unset( $meta_boxes );
}


/**
 * Check if meta boxes is included
 *
 * @return bool
 */
function theme_meta_box_maybe_include( $conditions ) {
	global $pagenow;

	/* don't save during quick edit */
	if ( $pagenow == 'admin-ajax.php' )
		return $post_id;

	/* don't save during autosave */
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		return $post_id;

	/* don't save if viewing a revision */
	// if ( $post_object->post_type == 'revision' )
	// 	return $post_id;

	if ( isset( $_GET['post'] ) )
		$post_id = $_GET['post'];
	elseif ( isset( $_POST['post_ID'] ) )
		$post_id = $_POST['post_ID'];
	else
		$post_id = false;

	$post_id = (int) $post_id;

	foreach ( $conditions as $cond => $v )
	{
		switch ( $cond )
		{
			case 'id':
				if ( !is_array( $v ) )
					$v = array( $v );
				return in_array( $post_id, $v );
			break;
			case 'slug':
				if ( !is_array( $v ) )
					$v = array( $v );
				$post = get_post( $post_id );
				return in_array( $post->post_name, $v );
			break;
			case 'template':
				if ( !is_array( $v ) )
					$v = array( $v );
				return in_array( get_post_meta( $post_id, '_wp_page_template', true ), $v );
			case 'function':
				if (!empty($v) AND function_exists('rw_'.$v))
					return call_user_func('rw_'.$v);
			case 'meta':
				if (is_array($v) AND (1==count($v)))
				{// We can have only one pair of meta-key and meta value to check
					$result = false;
					foreach ($v as $meta_key => $meta_value) {
						$result = ($meta_value==get_post_meta($post_id, $meta_key, true));
						if (!$result)
							break;
					}
					return $result;
				}
				elseif (false)
				{// We can have several pair of meta to check with conditional operator
					foreach ($v as $meta_key => $meta_value) {
						
					}
				}
				break;
			default:
				
				break;
		}
	}

	// If no condition matched
	return false;
}


/**
 * Check if page is current page
 *
 * @return bool
 */
function rw_is_page( $page_id ) {
	if ( !absint( $page_id ) )
		return false;

	$current_id = rw_get_post_id();

	return ( $current_id == $page_id );
}

/**
 * Check if page is custom_page_template
 *
 * @return bool
 */
function rw_is_custom_page_template( $template, $folder = 'page-templates' ) {
	$post_id = rw_get_post_id();

	if ( $folder )
		$template = "$folder/$template";

	if ( (string) get_page_template_slug( $post_id ) == "{$template}.php" )
		return true;

	return false; 
}

function rw_is_special_page( $key ) {
	$post_id = absint( get_theme_option( "{$key}_id" ) );

	return rw_is_page( $post_id );
}

function rw_get_post_id() {
	if ( isset( $_GET['post'] ) )
		return $_GET['post'];

	if ( isset( $_POST['post_ID'] ) )
		return $_POST['post_ID'];
	
	return null;
}

/**
 * Check if page is page_on_front
 *
 * @return bool
 */
function rw_is_front_page() {
	return true;
	$page_on_front = get_option( 'page_on_front' );
	return rw_is_page( $page_on_front );
}


/**
 * Check if page is about_page_template
 *
 * @return bool
 */
function rw_is_some_page_template() {
	return rw_is_custom_page_template( 'page-templates/some-page' );
}


function rw_is_child_page() {
	$post_id = rw_get_post_id();

	if ( !$post_id )
		return false;

	return !!wp_get_post_parent_id( $post_id );
}