<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Handle NUK Products
 */

class Inc_Module_Section extends Inc_Load_Section
{
	public static $section = 'modules';

	protected static $include_path;

	protected static $uri_path;

	public static $partials = array(
		// 'add-post-type-archive-to-menu',
		'option-tree',
		'theme-part-data',
		'simple-term-meta-new',
		'section-scroll-navigation',
		// 'user-meta',
	);

	protected static function get_partial( $partial = '' )
	{
		return $partial .'/'. $partial;
	}

	/**
	 * Define module constants
	 */
	public static function pre_load() 
	{
		if ( !defined( 'THEME_MODULE_FOLDER' ) )
			define( 'THEME_MODULE_FOLDER', self::$section );

		if ( !defined( 'THEME_MODULE_PATH' ) )
			define( 'THEME_MODULE_PATH', 'inc/'. THEME_MODULE_FOLDER );

		$uri = get_stylesheet_directory_uri() .'/'. THEME_MODULE_PATH;

		if ( !defined( 'THEME_MODULE_URI' ) )
			define( 'THEME_MODULE_URI', $uri );

		$abs = get_template_directory() .'/'. THEME_MODULE_PATH;

		if ( !defined( 'THEME_MODULE_ABS' ) )
			define( 'THEME_MODULE_ABS', $abs );
	}
}
Inc_Module_Section::load();