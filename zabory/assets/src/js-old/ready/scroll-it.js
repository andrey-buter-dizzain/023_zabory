var scrollItArgs;

/**
 * Fix scrollIt for small height block in the bottom of the page
 * Used if block in the bottom smaller than height of the window
 */
(function( $window, $document ){
	var timer;
	var $lastNav = $('[data-scroll-nav]:last');

	$window.scroll( function() {
		clearTimeout(timer);

		timer = setTimeout(function(){
			var bottom  = $document.height() - $window.height();
			var current = $window.scrollTop();

			if ( bottom != current ) 
				return;

			if ( $lastNav.hasClass( 'active' ) )
				return;

			// $lastNav
			// 	.addClass( 'active' )
			// 	.siblings()
			// 	.removeClass( 'active' );

			$lastNav
				.addClass( 'active' )
				.closest('li')
				.siblings()
				.find('a')
				.removeClass( 'active' );
		}, 100 );
	});

	if ( !scrollItArgs )
		scrollItArgs = {};

	$.scrollIt( scrollItArgs );

})( $(window), $(document) )

// scroll animation for governance/committees.php
// $('.block-item').click(function(){
// 	var index = +$(this).data('goto');
// 	$('html, body').animate({
// 		scrollTop: $('.block-text[data-index='+index+']').offset().top
// 		}, 600);
// });