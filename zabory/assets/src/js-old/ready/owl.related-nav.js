/**
 * Plugin Name
 * @since 2.0.0
 */
;(function ( $, window, document, undefined ) {
	RelatedNav = function(scope) {
		/**
		 * Reference to the core.
		 * @protected
		 * @type {Owl}
		 */
		this.owl = scope;
		
		/**
		 * The carousel element.
		 * @type {jQuery}
		 */
		this.$element = this.owl.$element;

		/**
		 * (Optional) The carousel parent element.
		 * @type {jQuery}
		 */
		this.$parent;

		/**
		 * The related navigation element.
		 * @type {jQuery}
		 */
		this.$nav = $();

		this.init();
	}

	RelatedNav.Defaults = {
		relatedNavItemClass:   'owl-related-nav-item',
		relatedNavParentClass: 'owl-carousel-parent',
		relatedNavId:          '',
	}
	
	RelatedNav.prototype.init = function() {
		var self = this;
		
		/**
		 * All event handlers.
		 * @protected
		 * @type {Object}
		 */
		this._handlers = {
			'initialized.owl.carousel': this.onInitialized.bind( this ),
			'translated.owl.carousel':  this.onTranslated.bind( this ),
		};

		// set default options
		this.owl.options = $.extend({}, RelatedNav.Defaults, this.owl.options);

		this.initNav();

		if ( 0 == this.$nav.length ) 
			return;

		// register event handlers
		this.$element.on( this._handlers );
		
		this.$nav.on( 'click', function(e) {
			e.preventDefault();

			self.onClickNav( $(this) );
		});
	}

	RelatedNav.prototype.onInitialized = function(e) {
		this.actvateNavItem( this.$nav.eq( 0 ) );
	}

	RelatedNav.prototype.onTranslated = function(e) {
		var key = 0;

		$.each( this.owl._items, function( i, item ) {
			if ( !$(item).hasClass( 'active' ) )
				return;

			key = i;

			return false;
		});

		this.actvateNavItem( this.$nav.eq( key ) );
	}

	RelatedNav.prototype.onClickNav = function( $item ) {
		this.actvateNavItem( $item );

		this.$element.trigger( 'to.owl.carousel', [ $item.index() ] );
	}

	RelatedNav.prototype.actvateNavItem = function( $item ) {
		$item
			.addClass( 'active' )
			.siblings( '.active' )
			.removeClass( 'active' );
	}

	RelatedNav.prototype.initNav = function() {
		var options = this.owl.options;

		if ( options.relatedNavId ) {
			this.$nav = $('#' + options.relatedNavId );
			return;
		}

		if ( !options.relatedNavParentClass ) 
			return;

		if ( !options.relatedNavItemClass ) 
			return;

		this.$parent = this.$element.parents( '.' + options.relatedNavParentClass );

		if ( 0 == this.$parent.length ) 
			return;

		this.$nav = this.$parent.find( '.' + options.relatedNavItemClass );
	}

	$.fn.owlCarousel.Constructor.Plugins.RelatedNav = RelatedNav;
})( window.Zepto || window.jQuery, window,  document );