$(document).on( 'click', function( e ) {
    var $el = $(e.currentTarget);

    gaOpenGallery( $el );
    gaOpenOrderForm( $el );
});

function gaOpenGallery( $el ) {
    var gallery = $el.data( 'gaGallery' );

    if ( ! gallery )
        return;

    ga( 'send', {
        hitType: 'event',
        eventCategory: 'gallery',
        eventAction: 'Homepage',
        eventLabel: gallery
    });
}

function gaOpenOrderForm( $el ) {
    var form = $el.data( 'gaForm' );

    if ( ! form )
        return;

    ga( 'send', {
        hitType: 'event',
        eventCategory: 'form',
        eventAction: 'Homepage',
        eventLabel: form
    } );
}