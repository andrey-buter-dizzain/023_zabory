/**
 * Main mathys theme js file
 */

"use strict";

(function ($, viewport) {
	var pixelRatio = !!window.devicePixelRatio ? window.devicePixelRatio : 1;

	var isRtl = false;

	jQuery(document).ready(function () {
		isRtl = 'undefined' != typeof icl_vars && icl_vars && 'ar' == icl_vars.current_language;
	});

	// var ThemeChart = function( el, data ) {
	// 	this.el   = el;
	// 	this.data = data;
	// }

	// ThemeChart.prototype.init = function() {
	// 	var chart = new google.visualization.ColumnChart( this.el );
	// 	chart.draw(this.getArgs(), this.getOptions());
	// }

	// ThemeChart.prototype.getArgs = function() {
	// 	var data = google.visualization.arrayToDataTable([
	// 		["Element", "Density", { role: "style" } ],
	// 		["Copper", 8.94, "#b87333"],
	// 		["Silver", 10.49, "silver"],
	// 		["Gold", 19.30, "gold"],
	// 		["Platinum", 21.45, "color: #e5e4e2"]
	// 	]);

	// 	var view = new google.visualization.DataView(data);
	// 	view.setColumns([0, 1,
	// 		{ calc: "stringify",
	// 			sourceColumn: 1,
	// 			type: "string",
	// 			role: "annotation" },
	// 	2]);

	// 	return view;
	// }

	// ThemeChart.prototype.getOptions = function() {
	// 	return {
	// 		// title: "Density of Precious Metals, in g/cm^3",
	// 		// width: 600,
	// 		// height: 400,
	// 		// bar: {groupWidth: "95%"},
	// 		// legend: { position: "none" },
	// 	};
	// }

	// google.charts.load("current", {packages:['corechart']});
	// google.charts.setOnLoadCallback(drawChart);
	// function drawChart() {


	// }

	// google.charts.load("current", '1', {packages:['corechart']});
	// google.charts.setOnLoadCallback(drawChart2);
	// function drawChart2() {
	// 	var data = google.visualization.arrayToDataTable([
	// 		["Element", "Density", { role: "style" } ],
	// 		["Copper", 8.94, "#b87333"],
	// 		["Silver", 10.49, "silver"],
	// 		["Gold", 19.30, "gold"],
	// 		["Platinum", 21.45, "color: #e5e4e2"]
	// 	]);

	// 	var view = new google.visualization.DataView(data);
	// 	view.setColumns([0, 1,
	// 		{ calc: "stringify",
	// 			sourceColumn: 1,
	// 			type: "string",
	// 			role: "annotation" },
	// 	2]);

	// 	var options = {
	// 		title: "Density of Precious Metals, in g/cm^3",
	// 		width: 600,
	// 		height: 400,
	// 		bar: {groupWidth: "95%"},
	// 		legend: { position: "none" },
	// 	};
	// 	var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values1"));
	// 	chart.draw(view, options);
	// }
	(function () {
		/*
   * Waits specified number of miliseconds before executing a callback
   */
		function changed(fn, ms) {
			var timer;
			return function () {
				clearTimeout(timer);
				timer = setTimeout(function () {
					fn();
				}, ms || 300);
			};
		}

		var $window = $(window);

		$window.on('resize', changed(function () {
			$window.trigger('themeResize');
		}));
	})();

	function setScrollBarMargin(set) {
		var margin = set ? scrollbarWidth() : 0;

		if ($(document).height() <= $(window).height()) margin = '';

		$('html').css('margin-right', margin);
		$('.stick-header-completed #site-header').css('padding-right', margin);
	}

	/**
  * Detect browser scrollbar width
  */
	function scrollbarWidth() {
		$(window).on('resize', function () {
			window.dizzainScrollBarWidth = null;
		});

		if (window.dizzainScrollBarWidth) return window.dizzainScrollBarWidth;

		var $inner = jQuery('<div style="width: 100%; height:200px;">test</div>'),
		    $outer = jQuery('<div style="width:200px;height:150px; position: absolute; top: 0; left: 0; visibility: hidden; overflow:hidden;"></div>').append($inner),
		    inner = $inner[0],
		    outer = $outer[0];

		var width1, width2;

		jQuery('body').append(outer);

		width1 = inner.offsetWidth;

		$outer.css('overflow', 'scroll');

		width2 = outer.clientWidth;

		$outer.remove();

		window.dizzainScrollBarWidth = width1 - width2;

		return window.dizzainScrollBarWidth;
	}

	function parseUrl(url) {
		var a = document.createElement('a');
		a.href = url;

		// a.protocol; // => "http:"
		// a.hostname; // => "example.com"
		// a.port; // => "3000"
		// a.pathname; // => "/pathname/"
		// a.search; // => "?search=test"
		// a.hash; // => "#hash"
		// a.host; // => "example.com:3000" 

		return a;
	}

	/**
  * Parse query string
  *
  * Tested:     test=1&class=2
  * Not tested: test=1&class
  *
  * @param  {string} query - NOT url, only query string
  * @return {object}
  */
	function parseQueryString(query) {
		var obj = {};

		if ('string' != typeof query) return obj;

		var vars = query.split('&');

		for (var i = 0; i < vars.length; i++) {
			var pair = vars[i].split('=');

			var key = decodeURIComponent(pair[0]);
			var val = decodeURIComponent(pair[1]);

			obj[key] = val;
		};

		return obj;
	}

	function getQueryVariable(variable) {
		var query = window.location.search.substring(1);

		var vars = query.split('&');

		for (var i = 0; i < vars.length; i++) {
			var pair = vars[i].split('=');
			if (decodeURIComponent(pair[0]) == variable) {
				return decodeURIComponent(pair[1]);
			}
		}

		console.log('Query variable %s not found', variable);
	}

	function getParameterByName(name, url) {
		if (!url) url = window.location.href;

		name = name.replace(/[\[\]]/g, "\\$&");

		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		    results = regex.exec(url);

		if (!results) return null;

		if (!results[2]) return '';

		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}

	function updateQueryStringParameter(uri, key, value) {
		var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
		var separator = uri.indexOf('?') !== -1 ? "&" : "?";
		if (uri.match(re)) {
			return uri.replace(re, '$1' + key + "=" + value + '$2');
		} else {
			return uri + separator + key + "=" + value;
		}
	}

	/**	
  * Detect iOS
  */
	function is_iOSmobile() {
		return navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false;
	}

	function isAndroid() {
		var ua = navigator.userAgent.toLowerCase();
		return ua.indexOf('android') > -1 /* && ua.indexOf( 'mobile' ) */;
	}

	// Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
	// see http://stackoverflow.com/questions/9847580/how-to-detect-safari-chrome-ie-firefox-and-opera-browser
	function isOpera() {
		return !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
	}

	// Firefox 1.0+
	function isFirefox() {
		return typeof InstallTrigger !== 'undefined';
	}

	// At least Safari 3+: "[object HTMLElementConstructor]"
	function isSafari() {
		return Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
	}

	// Chrome 1+
	function isChrome() {
		return !!window.chrome && !isOpera();
	}

	// At least IE6
	function isIE() {
		return (/*@cc_on!@*/false || !!document.documentMode
		);
	}

	function isMobileAndTabletDevice() {
		var check = false;
		(function (a) {
			if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
		})(navigator.userAgent || navigator.vendor || window.opera);
		return check;
	}

	function getIOSWindowHeight() {
		// Get zoom level of mobile Safari
		// Note, that such zoom detection might not work correctly in other browsers
		// We use width, instead of height, because there are no vertical toolbars :)
		var zoomLevel = document.documentElement.clientWidth / window.innerWidth;

		// window.innerHeight returns height of the visible area.
		// We multiply it by zoom and get out real height.
		return window.innerHeight * zoomLevel;
	};

	// You can also get height of the toolbars that are currently displayed
	function getHeightOfIOSToolbars() {
		var tH = (window.orientation === 0 ? screen.height : screen.width) - getIOSWindowHeight();
		return tH > 1 ? tH : 0;
	};

	/**
  * Generic functions to deal with cookies
  * чтобы удалить cookie, нужно создать новый с теми же параметрами, только c value=''
  */
	function createCookie(name, value, days) {
		var expires = "";

		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
			var expires = "; expires=" + date.toGMTString();
		}

		document.cookie = name + "=" + value + expires + "; path=/";
	}

	function readCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');

		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];

			// if first char of C is space
			while (c.charAt(0) == ' ') {
				c = c.substring(1, c.length);
			} // search for necessary elements
			if (c.indexOf(nameEQ) == 0) {
				c = decodeURIComponent(c.substring(nameEQ.length, c.length));
				c = c.replace('"', '');
				c = c.replace('"', '');
				return c;
			}
		}
		return undefined;
	}

	function updateCookie(name, value, days) {
		var cookie = readCookie(name);

		if (undefined !== cookie) {
			createCookie(name, '', -days);
		};
		createCookie(name, value, days);
	}

	function log(name, value) {
		var out = undefined !== value ? [name, value] : name;

		console.log(out);
	}
	$(document).ready(function () {
		/**
   * Plugin Name
   * @since 2.0.0
   */
		;(function ($, window, document, undefined) {
			RelatedNavSlider = function RelatedNavSlider(scope) {
				/**
     * Reference to the core.
     * @protected
     * @type {Owl}
     */
				this.owl = scope;

				/**
     * The carousel element.
     * @type {jQuery}
     */
				this.$element = this.owl.$element;

				/**
     * Related owl carousel.
     * @type {jQuery}
     */
				this.$relatedSlider;

				this.init();
			};

			RelatedNavSlider.Defaults = {
				$relatedSlider: null,
				relatedSliderStart: 0
			};

			RelatedNavSlider.prototype.init = function () {
				var self = this;

				// set default options
				this.owl.options = $.extend({}, RelatedNavSlider.Defaults, this.owl.options);

				if (!this.isSetSlider()) return;

				this.$relatedSlider = this.owl.options.$relatedSlider;

				this.setSlidesIndex();

				/**
     * All event handlers.
     * @protected
     * @type {Object}
     */
				this.$element.on({
					'initialized.owl.carousel': this.onInitialized.bind(this),
					'translated.owl.carousel': this.onTranslated.bind(this)
				});
			};

			RelatedNavSlider.prototype.setSlidesIndex = function () {
				var $slides = this.$element.find('> *');

				$slides.each(function (key, $item) {
					$(this).attr('data-index', key);
				});
			};

			RelatedNavSlider.prototype.isSetSlider = function () {
				var slider = this.owl.options.$relatedSlider;

				if (null === slider) return false;

				// if ( 'string' == typeof slider ) 
				// 	slider = $( slider );

				if (0 === slider.length) return false;

				return true;
			};

			RelatedNavSlider.prototype.onInitialized = function (e) {
				var start = this.owl.options.relatedSliderStart;

				this.setClickEvent(true);
				this.actvateNavItem($(this.owl._items[start]));
			};

			RelatedNavSlider.prototype.onTranslated = function (e) {
				this.setClickEvent();
			};

			RelatedNavSlider.prototype.setClickEvent = function (onLoad) {
				var query = '.' + this.owl.options.itemClass;

				if (!onLoad) query += '.active';

				var $slides = this.$element.find(query);
				var namespace = 'owl-related';
				var self = this;

				$slides.each(function (key, slide) {
					var events = $._data(slide, 'events');
					var isSetEvent = false;

					// find events for current item
					if (events && events.click) {
						$.each(events.click, function (key, event) {
							if (namespace == event.namespace) {
								isSetEvent = true;
							}
						});
					}

					if (isSetEvent) return;

					$(slide).on('click.' + namespace, self.onClickNav.bind(self));
				});
			};

			RelatedNavSlider.prototype.onClickNav = function (e) {
				e.preventDefault();

				var $item = $(e.currentTarget);

				this.actvateNavItem($item);

				this.$relatedSlider.trigger('to.owl.carousel', [$item.find('> *').data('index')]);
			};

			RelatedNavSlider.prototype.actvateNavItem = function ($item) {
				var cls = 'active-related';

				$item.addClass(cls).siblings('.' + cls).removeClass(cls);
			};

			$.fn.owlCarousel.Constructor.Plugins.RelatedNavSlider = RelatedNavSlider;
		})(window.Zepto || window.jQuery, window, document);
		/* mobile-menu */

		$('#primary-navigation').map(function () {
			var cl = 'mobile-menu-active';
			var mobilePoint = '<md';
			var index = null;

			var $primaryNav = $(this);
			var $body = $('body');
			var $mainBtn = $('#mobile-nav-toggler');

			$mainBtn.on('click', function (e) {
				e.preventDefault();

				toggleClasses(!$body.hasClass(cl));
			});

			$primaryNav.swipe({
				swipe: function swipe(event, direction) {
					if ('right' != direction) return;

					toggleClasses(false);
				}
			});

			$primaryNav.find('li').on('click', function () {
				if (!$body.hasClass(cl)) return;

				index = $(this).index();
			});

			// close mobile menu after scroll to needed section
			$body.on('theme-scroll-it', function (e, i) {
				if (null == index) return;

				if (index != i) return;

				if (!$body.hasClass(cl)) return;

				toggleClasses(!$body.hasClass(cl));

				index = null;
			});

			$(window).resize(viewport.changed(function () {
				if (viewport.is(mobilePoint)) return;

				if (!$body.hasClass(cl)) return;

				toggleClasses(false);
			}));

			function toggleClasses(toggle) {
				toggle = 'undefined' == typeof toggle ? true : toggle;

				$(this).toggleClass(cl, toggle);
				$body.toggleClass(cl, toggle);
			}
		});

		$('.section-products .hentry').each(function () {
			var $main = $(this);
			var $gallery = $main.find('.product-popup-gallery');
			var $galleryBtn = $main.find('.open-popup-gallery');

			$galleryBtn.on('click', function (e) {
				e.preventDefault();

				$gallery.children('a').eq(0).trigger('click');
			});

			$gallery.magnificPopup({
				delegate: 'a',
				type: 'image',
				gallery: {
					enabled: true
				}
			});
		});

		$('.js-open-popup-link').magnificPopup({
			type: 'inline',
			closeOnBgClick: true,
			midClick: true
			// callbacks: {
			// 	elementParse: function( item ) {
			// 		var owlSpan = '<span class="table"><span class="cell"></span></span>';

			// 		$(item.src).find('.owl-carousel-large').owlCarousel({
			// 			items:1,
			// 			loop:true,
			// 			nav: true,
			// 			navText: [ owlSpan, owlSpan ],
			// 			dots: false,
			// 			mouseDrag: false,
			// 			onTranslated: function( e ) {
			// 			},
			// 			onInitialized: function( e ) {
			// 			}
			// 		});

			// 		$(item.src).find('.owl-carousel-nav').owlCarousel({
			// 			loop:true,
			// 			nav: true,
			// 			navText: [ owlSpan, owlSpan ],
			// 			dots: false,
			// 			mouseDrag: false,
			// 			$relatedSlider: $(item.src).find('.owl-carousel-large'),
			// 			onTranslated: function( e ) {
			// 			},
			// 			onInitialized: function( e ) {
			// 			}
			// 		});
			// 	},
			// }
		});

		var scrollItArgs = {
			topOffset: -72,
			onPageChange: function onPageChange(index, active) {

				// $body.trigger( 'theme-scroll-it', [index] );
			}
		};

		$('input[type=tel]').mask('+375 (99) 999-99-99');

		$('.wpcf7').each(function () {
			var $main = $(this);

			$main.on('wpcf7:mailsent', function () {
				console.log('wpcf7:mailsent');
				$main.addClass('hidden');
				$main.closest('.post-popup-wrapper').find('.thank-message').removeClass('hidden');
			});
		});

		$('#site-header').each(function () {
			var $main = $(this);
			var $document = $(document);

			$document.scroll(function (e) {
				$main.css({
					'left': -$document.scrollLeft()
				});
			});
		});

		$('.js-section-top-carousel').owlCarousel({
			items: 1,
			loop: true,
			autoplay: true,
			autoplayTimeout: 5000,
			// autoplaySpeed: true,
			mouseDrag: false,
			touchDrag: false,
			pullDrag: false,
			dots: false,
			nav: false,
			animateOut: 'fadeOut'
		});

		$(document).on('click', function (e) {
			var $el = $(e.currentTarget);

			gaOpenGallery($el);
			gaOpenOrderForm($el);
		});

		function gaOpenGallery($el) {
			var gallery = $el.data('gaGallery');

			if (!gallery) return;

			ga('send', {
				hitType: 'event',
				eventCategory: 'gallery',
				eventAction: 'Homepage',
				eventLabel: gallery
			});
		}

		function gaOpenOrderForm($el) {
			var form = $el.data('gaForm');

			if (!form) return;

			ga('send', {
				hitType: 'event',
				eventCategory: 'form',
				eventAction: 'Homepage',
				eventLabel: form
			});
		}
		var scrollItArgs;

		/**
   * Fix scrollIt for small height block in the bottom of the page
   * Used if block in the bottom smaller than height of the window
   */
		(function ($window, $document) {
			var timer;
			var $lastNav = $('[data-scroll-nav]:last');

			$window.scroll(function () {
				clearTimeout(timer);

				timer = setTimeout(function () {
					var bottom = $document.height() - $window.height();
					var current = $window.scrollTop();

					if (bottom != current) return;

					if ($lastNav.hasClass('active')) return;

					// $lastNav
					// 	.addClass( 'active' )
					// 	.siblings()
					// 	.removeClass( 'active' );

					$lastNav.addClass('active').closest('li').siblings().find('a').removeClass('active');
				}, 100);
			});

			if (!scrollItArgs) scrollItArgs = {};

			$.scrollIt(scrollItArgs);
		})($(window), $(document));

		// scroll animation for governance/committees.php
		// $('.block-item').click(function(){
		// 	var index = +$(this).data('goto');
		// 	$('html, body').animate({
		// 		scrollTop: $('.block-text[data-index='+index+']').offset().top
		// 		}, 600);
		// });
	});
	$(window).on("load", function () {});
})(jQuery, ResponsiveBootstrapToolkit);
