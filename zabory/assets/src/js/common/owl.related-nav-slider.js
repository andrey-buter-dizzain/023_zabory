/**
 * Plugin Name
 * @since 2.0.0
 */

export default () => {
	 ;(function ( $, window, document, undefined ) {
		var RelatedNavSlider = function(scope) {
			 /**
			  * Reference to the core.
			  * @protected
			  * @type {Owl}
			  */
			 this.owl = scope;
			 
			 /**
			  * The carousel element.
			  * @type {jQuery}
			  */
			 this.$element = this.owl.$element;
	
			 /**
			  * Related owl carousel.
			  * @type {jQuery}
			  */
			 this.$relatedSlider;
	
			 this.init();
		 }
	
		 RelatedNavSlider.Defaults = {
			 $relatedSlider: null,
			 relatedSliderStart: 0
		 }
		 
		 RelatedNavSlider.prototype.init = function() {
			 var self = this;
			 
			 // set default options
			 this.owl.options = $.extend({}, RelatedNavSlider.Defaults, this.owl.options);
	
			 if ( ! this.isSetSlider() ) 
				 return;
	
			 this.$relatedSlider = this.owl.options.$relatedSlider;
	
			 this.setSlidesIndex();
	
			 /**
			  * All event handlers.
			  * @protected
			  * @type {Object}
			  */
			 this.$element.on({
				 'initialized.owl.carousel': this.onInitialized.bind( this ),
				 'translated.owl.carousel':  this.onTranslated.bind(  this ),
			 });
		 }
	
		 RelatedNavSlider.prototype.setSlidesIndex = function() {
			 var $slides = this.$element.find( '> *' );
	
			 $slides.each( function( key, $item ) {
				 $(this).attr( 'data-index', key );
			 });
		 }
	
		 RelatedNavSlider.prototype.isSetSlider = function() {
			 var slider = this.owl.options.$relatedSlider;
	
			 if ( null === slider ) 
				 return false;
	
			 // if ( 'string' == typeof slider ) 
			 // 	slider = $( slider );
	
			 if ( 0 === slider.length ) 
				 return false;
	
			 return true;
		 }
	
		 RelatedNavSlider.prototype.onInitialized = function(e) {
			 var start = this.owl.options.relatedSliderStart;
	
			 this.setClickEvent( true );
			 this.actvateNavItem( $( this.owl._items[ start ] ) );
		 }
	
		 RelatedNavSlider.prototype.onTranslated = function(e) {
			 this.setClickEvent();
		 }
	
		 RelatedNavSlider.prototype.setClickEvent = function( onLoad ) {
			 var query = '.' + this.owl.options.itemClass;
	
			 if ( !onLoad ) 
				 query += '.active';
	
			 var $slides = this.$element.find( query );
			 var namespace = 'owl-related';
			 var self = this;
	
			 $slides.each( function( key, slide ) {
				 var events = $._data( slide, 'events' );
				 var isSetEvent = false;
	
				 // find events for current item
				 if ( events && events.click ) {
					 $.each( events.click, function( key, event ) {
						 if ( namespace == event.namespace ) {
							 isSetEvent = true;
						 }
					 });
				 }
	
				 if ( isSetEvent ) 
					 return;
	
				 $( slide ).on( 'click.' + namespace, self.onClickNav.bind( self ) );
			 });
		 }
	
		 RelatedNavSlider.prototype.onClickNav = function( e ) {
			 e.preventDefault();
	
			 var $item = $( e.currentTarget )
	
			 this.actvateNavItem( $item );
	
			 this.$relatedSlider.trigger( 'to.owl.carousel', [ $item.find( '> *' ).data( 'index' ) ] );
		 }
	
		 RelatedNavSlider.prototype.actvateNavItem = function( $item ) {
			 var cls = 'active-related';
	
			 $item
				 .addClass( cls )
				 .siblings( '.' + cls )
				 .removeClass( cls );
		 }
	
		 $.fn.owlCarousel.Constructor.Plugins.RelatedNavSlider = RelatedNavSlider;
	 })( window.Zepto || window.jQuery, window,  document );
 }
