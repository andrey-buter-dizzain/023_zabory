const $ = jQuery;
const viewport = ResponsiveBootstrapToolkit;

export default () => {
	$('#primary-navigation').map(function() {
		var cl = 'mobile-menu-active';
		var mobilePoint = '<md';
		var index = null;
		
		var $primaryNav = $(this);
		var $body       = $('body');
		var $mainBtn    = $('#mobile-nav-toggler');

		$mainBtn.on( 'click', function(e) {
			e.preventDefault();

			toggleClasses( !$body.hasClass( cl ) );
		});

		$primaryNav.swipe({
			swipe: function( event, direction ) {
				if ( 'right' != direction )
					return;

				toggleClasses(false);
			},
		});

		$primaryNav.find( 'li' ).on( 'click', function() {
			if ( !$body.hasClass( cl ) ) 
				return;

			index = $(this).index();
		});

		// close mobile menu after scroll to needed section
		$body.on( 'theme-scroll-it', function( e, i ) {
			if ( null == index )
				return;

			if ( index != i ) 
				return;

			if ( !$body.hasClass( cl ) ) 
				return;

			toggleClasses( !$body.hasClass( cl ) );

			index = null;
		});

		$(window).resize(
			viewport.changed( function() {
				if ( viewport.is( mobilePoint ) ) 
					return;

				if ( !$body.hasClass( cl ) ) 
					return;

				toggleClasses(false);
			})
		);

		function toggleClasses( toggle ) {
			toggle = ( 'undefined' == typeof toggle ) ? true : toggle;

			$(this).toggleClass( cl, toggle );
			$body.toggleClass( cl, toggle );		
		}
	});
}