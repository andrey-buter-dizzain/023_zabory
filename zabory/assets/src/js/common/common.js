const $ = jQuery;

export default () => {
	$('.section-products .hentry').each( function() {
		var $main    = $(this);
		var $gallery = $main.find( '.product-popup-gallery' );
		var $galleryBtn  = $main.find('.open-popup-gallery');
	
		$galleryBtn.on( 'click', function( e ) {
			e.preventDefault();
	
			$gallery.children( 'a' ).eq( 0 ).trigger( 'click' );
		});
	
		$gallery.magnificPopup({
			delegate: 'a',
			type: 'image',
			gallery: {
				enabled:true
			}
		});
	});
	
	$('.js-open-popup-link').magnificPopup({
		type:'inline',
		closeOnBgClick: true,
		midClick: true, 
		// callbacks: {
		// 	elementParse: function( item ) {
		// 		var owlSpan = '<span class="table"><span class="cell"></span></span>';
	
		// 		$(item.src).find('.owl-carousel-large').owlCarousel({
		// 			items:1,
		// 			loop:true,
		// 			nav: true,
		// 			navText: [ owlSpan, owlSpan ],
		// 			dots: false,
		// 			mouseDrag: false,
		// 			onTranslated: function( e ) {
		// 			},
		// 			onInitialized: function( e ) {
		// 			}
		// 		});
	
		// 		$(item.src).find('.owl-carousel-nav').owlCarousel({
		// 			loop:true,
		// 			nav: true,
		// 			navText: [ owlSpan, owlSpan ],
		// 			dots: false,
		// 			mouseDrag: false,
		// 			$relatedSlider: $(item.src).find('.owl-carousel-large'),
		// 			onTranslated: function( e ) {
		// 			},
		// 			onInitialized: function( e ) {
		// 			}
		// 		});
		// 	},
		// }
	});
	
	var scrollItArgs = {
		topOffset: -72,
		onPageChange: function( index, active ) {
			
			// $body.trigger( 'theme-scroll-it', [index] );
		}
	};
	
	
	$('input[type=tel]').mask('+375 (99) 999-99-99');
	
	$('.wpcf7').each( function() {
		var $main = $(this);
	
		$main.on( 'wpcf7:mailsent', function() {
			console.log( 'wpcf7:mailsent' );
			$main.addClass( 'hidden' );
			$main
				.closest( '.post-popup-wrapper' )
				.find( '.thank-message' )
				.removeClass( 'hidden' );
		});
	});
	
	// $('#site-header').each( function() {
	// 	var $main     = $(this);
	// 	var $document = $(document);
	
	// 	$document.scroll(function(e) {
	// 		$main.css({
	// 			'left': - $document.scrollLeft()
	// 		});
	// 	})
	// });
	
	$('.js-section-top-carousel').owlCarousel({
		items: 1,
		loop: true,
		autoplay: true,
		autoplayTimeout: 5000,
		// autoplaySpeed: true,
		mouseDrag: false,
		touchDrag: false,
		pullDrag: false,
		dots: false,
		nav: false,
		animateOut: 'fadeOut'
	})
	
}


