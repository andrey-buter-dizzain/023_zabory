export default {
    intersectArrays: (left, right) => {
        return left.filter(function (el) {
            return (-1 !== right.indexOf(el));
        });
    },
    setScrollBarMargin: function(set) {
        var margin = set ? this.scrollbarWidth() : 0;

        if ($(document).height() <= $(window).height())
            margin = '';

        $('html').css('margin-right', margin);
        $('.stick-header-completed #site-header').css('padding-right', margin);
    },
    scrollbarWidth: () => {
        jQuery(window).on('resize', function () {
            window.browserScrollBarWidth = null;
        });

        if (window.browserScrollBarWidth)
            return window.browserScrollBarWidth;

        var $inner = jQuery('<div style="width: 100%; height:200px;">test</div>'),
            $outer = jQuery('<div style="width:200px;height:150px; position: absolute; top: 0; left: 0; visibility: hidden; overflow:hidden;"></div>').append($inner),
            inner = $inner[0],
            outer = $outer[0];

        var width1,
            width2;

        jQuery('body').append(outer);

        width1 = inner.offsetWidth;

        $outer.css('overflow', 'scroll');

        width2 = outer.clientWidth;

        $outer.remove();

        window.browserScrollBarWidth = (width1 - width2);

        return window.browserScrollBarWidth;
    },
    parseUrl: (url) => {
        var a = document.createElement('a');
        a.href = url;

        // a.protocol; // => "http:"
        // a.hostname; // => "example.com"
        // a.port; // => "3000"
        // a.pathname; // => "/pathname/"
        // a.search; // => "?search=test"
        // a.hash; // => "#hash"
        // a.host; // => "example.com:3000" 

        return a;
    },
    getUrlQuery: function() {
        var query = window.location.search.substring(1);

        if (!query)
            return {};

        return this.parseQueryString(query);
    },
    getUrlWithoutQuery: () => {
        return window.location.href.split('?')[0];
    },
    /**
     * Parse query string
     *
     * Tested:     test=1&class=2
     * Not tested: test=1&class
     *
     * @param  {string} query - NOT url, only query string
     * @return {object}
     */
    parseQueryString: (query) => {
        var obj = {};

        if ('string' != typeof query)
            return obj;

        var vars = query.split('&');

        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=');

            var key = decodeURIComponent(pair[0]);
            var val = decodeURIComponent(pair[1]);

            obj[key] = val;
        };

        return obj;
    },
    getQueryVariable: (variable) => {
        var query = window.location.search.substring(1);

        var vars = query.split('&');

        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=');
            if (decodeURIComponent(pair[0]) == variable) {
                return decodeURIComponent(pair[1]);
            }
        }

        console.log('Query variable %s not found', variable);
    },
    getParameterByName: (name, url) => {
        if (!url)
            url = window.location.href;

        name = name.replace(/[\[\]]/g, "\\$&");

        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);

        if (!results)
            return null;

        if (!results[2])
            return '';

        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
    updateQueryStringParameter: () => {
        var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
        var separator = uri.indexOf('?') !== -1 ? "&" : "?";
        if (uri.match(re)) {
            return uri.replace(re, '$1' + key + "=" + value + '$2');
        }
        else {
            return uri + separator + key + "=" + value;
        }
    },
    urlParam: (name) => {
        let results = new RegExp(`[\?&]${name}=([^&#]*)`).exec(window.location.href);
        if (null == results)
            return null;

        return results[1] || 0;
    },
    loadJsCssFile: (filename, filetype) => {
        if ( 'js' == filetype) { //if filename is a external JavaScript file
            var fileref = document.createElement('script')
            fileref.setAttribute("type", "text/javascript")
            fileref.setAttribute("src", filename)
        }
        else if ('css' == filetype) { //if filename is an external CSS file
            var fileref = document.createElement("link")
            fileref.setAttribute("rel", "stylesheet")
            fileref.setAttribute("type", "text/css")
            fileref.setAttribute("href", filename)
        }
        if ( 'undefined' != typeof fileref)
            document.getElementsByTagName("head")[0].appendChild(fileref)
    }
};