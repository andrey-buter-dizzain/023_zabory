import common from "./common/common";
import ga from "./common/ga-events";
import nav from "./common/mobile-primary-navigation";
import owlRelated from "./common/owl.related-nav-slider";
import scrollIt from "./common/scroll-it";

const $ = jQuery;

$( document ).ready( () => {
    common();
    ga();
    nav();
    scrollIt();
    owlRelated();
} );