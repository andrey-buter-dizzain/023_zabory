<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$section = basename( __FILE__, '.php' );

$thumb_id = get_post_thumbnail_id( get_the_ID(), 'full' );

$gallery = get_post_meta( get_the_ID(), 'page_top_gallery', true );

$gallery = $gallery ? explode( ',', $gallery ) : array();

if ( $thumb_id ) {
	array_unshift( $gallery, $thumb_id );
}

?>
<section id="section-<?php echo $section ?>" <?php module_scroll_data_index( $section ) ?> style="background-image:url(<?php echo $src ?>)">
	<?php if ( $gallery ): ?>
		<div class="owl-carousel section-top-carousel js-section-top-carousel">
			<?php foreach ( $gallery as $id ): 
				$src = get_theme_image_src( $id, 'full' );
			?>
				<div class="owl-slide" style="background-image:url(<?php echo $src ?>)"></div>
			<?php endforeach ?>
		</div>
	<?php endif ?>

	<div class="wrapper">
		<div class="section-content">
			<div class="section-content-inn">
				<h1 class="section-title">
					<?php the_title() ?>
				</h1>

				<div class="section-text">
					<?php the_content() ?>
				</div>
			</div>
		</div>
	</div>
</section>