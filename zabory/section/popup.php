<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$data = get_theme_part_data();

if ( ! $data )
	return;

if ( ! $data['id'] )
	return;

if ( ! $data['form_id'] )
	return;

$form = get_post( absint( $data['form_id'] ) );

?>
<div id="<?php echo $data['id'] ?>" class="wrapper post-popup-wrapper">
	<div class="post-popup-header">
		<div class="post-popup-title">
			<?php if ( $data['title'] ): ?>
				<?php echo $data['title'] ?>
			<?php else: ?>
				<?php echo $form->post_title ?>
			<?php endif ?>
		</div>

		<button class="post-popup-close mfp-close" title="<?php _e( 'Закрыть окно' ) ?>">×</button>
	</div>
	<div class="post-popup-content-wrapper">
		<?php echo do_shortcode( '[contact-form-7 id="'. $form->ID .'" title="'. $form->post_title .'"]' ) ?>
	</div>

	<?php if ( $data['thank'] ): ?>
		<div class="thank-message hidden">
			<?php echo $data['thank'] ?>
		</div>
	<?php endif ?>
</div>