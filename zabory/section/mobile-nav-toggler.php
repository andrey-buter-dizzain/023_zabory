<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); ?>

<div id="mobile-nav-toggler-wrapper">
	<div id="mobile-nav-toggler">
		<span></span>
	</div>
</div>