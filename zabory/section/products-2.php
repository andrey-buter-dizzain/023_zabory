<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$section = basename( __FILE__, '.php' );

$title = get_post_meta( get_the_ID(), "page_products_2_title", true );
$cat   = get_post_meta( get_the_ID(), "page_products_2_cat", true );

if ( !$cat )
	return;

$args = array(
	'post_type'      => 'post',
	'posts_per_page' => -1,
	'post_status'    => array( 'publish' ),
	'cat'            => $cat
);

if ( is_user_logged_in() ) {
	$args['post_status'][] = 'draft';
}

query_posts( $args );

if ( !have_posts() )
	return wp_reset_query();

?>
<section id="section-<?php echo $section ?>" class="section-block section-products" <?php module_scroll_data_index( $section ) ?>>
	<div class="wrapper">
		<h2 class="section-title">
			<?php echo $title ?>
		</h2>

		<div class="products-block row row-flex"><?php 
			while ( have_posts() ): the_post();
				get_theme_part( 'content', 'product-2' );
			endwhile;
		?></div>
	</div>
</section>
<?php wp_reset_query();