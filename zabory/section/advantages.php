<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
$section = basename( __FILE__, '.php' );

$title = get_post_meta( get_the_ID(), "page_{$section}_title", true );
$data  = get_post_meta( get_the_ID(), "page_{$section}_data", true );

?>
<section id="section-<?php echo $section ?>" class="section-block" <?php module_scroll_data_index( $section ) ?>>
	<div class="wrapper">
		<h2 class="section-title">
			<?php echo $title ?>
		</h2>

		<?php if ( $data ): ?>
			<div class="row row-inline"><?php 
				foreach ( $data as $key => $item ): 
					?><div class="advantage-item <?php echo $item['class'] ?> col-xs-4">
						<div class="icon"></div>

						<div class="content">
							<div class="title">
								<?php echo $item['title'] ?>
							</div>
							<div class="text">
								<?php echo $item['text'] ?>
							</div>
						</div>
					</div><?php 
				endforeach;
			?></div>
		<?php endif ?>
	</div>
</section>