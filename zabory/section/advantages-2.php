<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
$section = basename( __FILE__, '.php' );

$title  = get_post_meta( get_the_ID(), "page_{$section}_title", true );
$data   = get_post_meta( get_the_ID(), "page_{$section}_data", true );
$button = get_post_meta( get_the_ID(), "page_{$section}_button", true );
$form   = get_post_meta( get_the_ID(), "page_{$section}_form", true );

?>
<section id="section-<?php echo $section ?>" class="section-block" <?php module_scroll_data_index( $section ) ?>>
	<div class="wrapper">
		<h2 class="section-title">
			<?php echo $title ?>
		</h2>

		<?php if ( $data ): ?>
			<div class="row row-inline"><?php 
				foreach ( $data as $key => $item ): 
					?><div class="advantage-item <?php echo $item['class'] ?> col-xs-6">
						<div class="icon"></div>

						<div class="content">
							<div class="title">
								<?php echo $item['title'] ?>
							</div>
							<div class="text">
								<?php echo $item['text'] ?>
							</div>
						</div>
					</div><?php 
				endforeach;
			?></div>
		<?php endif ?>

		<?php if ( $form ): ?>
			<div class="button-block">
				<a class="button call-popup js-open-popup-link" href="#call-us-popup"><?php echo $button ?></a>
			</div>
			
			<div style="display:none;">
				<?php get_theme_part( 'section', 'popup', array(
					'data' => array(
						'id'      => 'call-us-popup',
						'form_id' => $form,
						'thank'   => '
							<p class="green">Заявка на звонок успешно оформлена.</p>
							<p>Наш менеджер свяжется с Вами <br>в ближайшее время.</p>
						',
					)
				) ) ?>
			</div>
		<?php endif ?>


	</div>
</section>