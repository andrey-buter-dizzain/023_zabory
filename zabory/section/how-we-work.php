<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
$section = basename( __FILE__, '.php' );

$title  = get_post_meta( get_the_ID(), "page_work_title", true );
$data   = get_post_meta( get_the_ID(), "page_work_data", true );
$button = get_post_meta( get_the_ID(), "page_work_button", true );
$form   = get_post_meta( get_the_ID(), "page_work_form", true );

if ( ! $data )
	return;

?>
<section id="section-<?php echo $section ?>" class="section-block" <?php module_scroll_data_index( $section ) ?>>
	<div class="wrapper">
		<h2 class="section-title">
			<?php echo $title ?>
		</h2>

		<div class="section-data row">
			<?php foreach ( $data as $item ): ?>
				<div class="col-xs-12 col-md-4 section-data-item <?php echo $item['class'] ?>">
					<div class="title">
						<?php echo $item['title'] ?>
					</div>
					<div class="content">
						<?php echo wpautop( $item['text'] ) ?>
					</div>
				</div>
			<?php endforeach ?>
		</div>

		<?php if ( $form ): ?>
			<div class="button-block">
				<a class="button call-popup js-open-popup-link" href="#call-us-popup-2"><?php echo $button ?></a>
			</div>
			
			<div style="display:none;">
				<?php get_theme_part( 'section', 'popup', array(
					'data' => array(
						'id'      => 'call-us-popup-2',
						'form_id' => $form,
						'thank'   => '
							<p class="green">Заявка на звонок успешно оформлена.</p>
							<p>Наш менеджер свяжется с Вами <br>в ближайшее время.</p>
						',
					)
				) ) ?>
			</div>
		<?php endif ?>
	</div>
</section>