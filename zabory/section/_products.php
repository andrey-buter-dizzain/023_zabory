<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
$section = basename( __FILE__, '.php' );

$title = get_post_meta( get_the_ID(), "page_{$section}_title", true );

$args = array(
	'post_type'      => 'post',
	'posts_per_page' => -1
);

query_posts( $args );

if ( !have_posts() )
	return wp_reset_query();

?>
<section id="section-<?php echo $section ?>" class="section-block" <?php module_scroll_data_index( $section ) ?>>
	<div class="wrapper">
		<h2 class="section-title">
			<?php echo $title ?>
		</h2>

		<div class="products-block row row-inline"><?php 
			while ( have_posts() ): the_post();
				get_theme_part( 'content', 'product' );
			endwhile;
		?></div>
	</div>
</section>
<?php wp_reset_query();