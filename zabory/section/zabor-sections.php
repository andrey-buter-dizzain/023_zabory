<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
$section = basename( __FILE__, '.php' );

$title = get_post_meta( get_the_ID(), "page_{$section}_title", true );
$text  = get_post_meta( get_the_ID(), "page_{$section}_text", true );
$image = get_post_meta( get_the_ID(), "page_{$section}_image", true );

$src = get_theme_option_image_src( $image, 'full' );

?>
<section id="section-<?php echo $section ?>" class="section-block" <?php module_scroll_data_index( $section ) ?> style="background-image:url(<?php echo $src ?>)">
	<div class="wrapper">
		<h2 class="section-title">
			<?php echo $title ?>
		</h2>

		<div class="section-text">
			<?php echo wpautop( $text ) ?>
		</div>
	</div>
</section>