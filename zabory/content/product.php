<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * The default template for displaying single post content
 *
 * @package WordPress
 * @subpackage Dizzain_Blog
 * @since Dizzain_Blog 1.0
 */

$desc   = get_post_meta( get_the_ID(), 'post_popup_desc', true );
$images = get_post_meta( get_the_ID(), 'post_popup_gallery', true );

$class = empty( $images ) ? '' : 'has-gallery';

?><article id="post-<?php the_ID(); ?>" <?php post_class( 'col-xs-6' ); ?>>
	<div class="entry-header">
		<h4 class="entry-title">
			<?php the_title() ?>
		</h4>
		<?php if ( has_excerpt() ): ?>
			<div class="entry-price">
				<?php the_excerpt() ?>
			</div>
		<?php endif ?>
	</div>
	<div class="entry-thumbnail">
		<?php the_post_thumbnail( 'product' ) ?>
	</div>
	<div class="entry-block">

		<div class="entry-content">
			<?php the_content() ?>
		</div>

		<a href="#post-description-<?php the_ID() ?>" class="more-link js-open-popup-link"><?php _e( 'Подробнее' ) ?></a>

		<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<span class="edit-link">', '</span>' ); ?>
	</div>

	<div style="display:none">
		<div id="post-description-<?php the_ID() ?>" class="wrapper post-popup-description <?php echo $class ?>">
			<div class="post-popup-header">
				<div class="post-popup-title">
					<?php the_title() ?>
				</div>

				<?php if ( has_excerpt() ): ?>
					<div class="post-popup-price">
						<?php the_excerpt() ?>
					</div>
				<?php endif ?>

				<button class="post-popup-close mfp-close" title="<?php _e( 'Закрыть окно' ) ?>">×</button>
			</div>
			<div class="post-popup-content-wrapper">
				<div class="post-popup-content">
					<?php the_content() ?>
				</div>
				<div class="post-popup-description">
					<?php echo wpautop( $desc ) ?>
				</div>

				<?php echo do_shortcode( '[contact-form-7 id="37" title="Contact form 1"]' ) ?>
			</div>

			<?php if ( $images ): ?>
				<?php get_theme_part( 'content/product', 'gallery', array(
					'data' => $images
				) ) ?>
			<?php endif ?>
		</div>
	</div>

</article><?php 