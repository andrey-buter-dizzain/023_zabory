<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * The template for displaying a "No posts found" message
 *
 * @package WordPress
 * @subpackage Socialflow
 * @since Socialflow 1.0
 */

?>
<div class="hentry hentry-404">
	<div class="entry-content">
		<h1 class="page-title"><?php theme_option( '404_title' ) ?></h1>
		<?php echo wpautop( get_theme_option( '404_content' ) ) ?>
	</div>
</div>