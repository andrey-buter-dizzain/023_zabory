<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * The default template for displaying single post content
 *
 * @package WordPress
 * @subpackage Dizzain_Blog
 * @since Dizzain_Blog 1.0
 */

?><article id="post-<?php the_ID(); ?>" <?php post_class( 'col-xs-12 col-sm-6 col-md-4' ); ?>>
	<div class="hentry-inn">
		<div class="entry-thumbnail">
			<?php the_post_thumbnail( 'product' ) ?>
		</div>

		<h4 class="entry-title">
			<?php the_title() ?>
		</h4>
	</div>
</article><?php 