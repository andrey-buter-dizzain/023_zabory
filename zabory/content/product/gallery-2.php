<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$ids = get_theme_part_data();

if ( ! $ids )
	return;

$args = array(
	'post_type' => 'attachment',
	'post__in'  => explode( ',', $ids ),
	'orderby'   => 'post__in',
	'posts_per_page' => -1
);

$posts = get_posts( $args );

if ( ! $posts )
	return;

?>
<div class="product-popup-gallery">
	<?php foreach ( $posts as $key => $post ): ?>
		<a href="<?php echo get_theme_image_src( $post->ID, 'large' ) ?>">
			<?php echo $key ?>
		</a>
	<?php endforeach ?>
</div>