<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$ids = get_theme_part_data();

if ( ! $ids )
	return;

$args = array(
	'post_type' => 'attachment',
	'post__in'  => explode( ',', $ids ),
	'orderby'   => 'post__in',
	'posts_per_page' => -1
);

$posts = get_posts( $args );

if ( ! $posts )
	return;

$nav = '';

?>
<div class="post-popup-gallery-main owl-carousel-parent">
	<div class="post-popup-gallery owl-carousel owl-carousel-large">
		<?php foreach ( $posts as $key => $post ): ?>
			<div class="gallery-item">
				<?php echo wp_get_attachment_image( $post->ID, 'large' ) ?>
			</div>
		<?php endforeach ?>
	</div>
	<div class="post-popup-gallery-nav owl-carousel owl-carousel-nav">
		<?php foreach ( $posts as $key => $post ): ?>
			<div class="gallery-nav-item owl-related-nav-item">
				<?php echo wp_get_attachment_image( $post->ID, 'thumbnail' ) ?>
			</div>
		<?php endforeach ?>
	</div>
</div>