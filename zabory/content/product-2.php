<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * The default template for displaying single post content
 *
 * @package WordPress
 * @subpackage Dizzain_Blog
 * @since Dizzain_Blog 1.0
 */

$data   = get_post_meta( get_the_ID(), 'post_popup_data', true );
$desc   = get_post_meta( get_the_ID(), 'post_popup_desc', true );
$images = get_post_meta( get_the_ID(), 'post_popup_gallery', true );
$form   = get_post_meta( get_the_ID(), 'post_popup_form', true );
$ga     = get_post_meta( get_the_ID(), 'post_ga_name', true );

$class = empty( $images ) ? '' : 'has-gallery';

?><article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-header">
		<h4 class="entry-title">
			<?php the_title() ?>
		</h4>
		<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<span class="edit-link">', '</span>' ); ?>
	</div>
	<div class="entry-thumbnail">
		<a class="open-popup-gallery" href="#" data-ga-gallery="<?php echo $ga ?>">
			<?php the_post_thumbnail( 'product-2' ) ?>
		</a>
	</div>
	<div class="entry-block">

		<div class="entry-content">
			<?php //the_content() ?>

			<?php if ( $data ): ?>
				<?php foreach ( $data as $item ): 
					if ( ! $item )
						continue;
						
					$text = explode( "\n", $item['text'] );
				?>
					<div class="content-item">
						<div class="header table">
							<div class="title table-item">
								<?php echo $item['title'] ?>
							</div>
							<?php if ( $item['title-attr'] ): ?>
								<div class="title-attr table-item">
									<?php echo $item['title-attr'] ?>
								</div>
							<?php endif ?>
						</div>
						<?php foreach ( $text as $line ): 
							$vals = explode( '|', $line );
						?>
							<div class="item table">
								<div class="name table-item">
									<?php echo $vals[0] ?>
								</div>
								<?php if ( isset( $vals[1] ) ): ?>
									<div class="value table-item">
										<?php echo $vals[1] ?>
									</div>
								<?php endif ?>
							</div>
						<?php endforeach ?>
					</div>
				<?php endforeach ?>
			<?php endif ?>
		</div>

		<?php if ( has_excerpt() ): ?>
			<div class="entry-price">
				<?php the_excerpt() ?>
			</div>
		<?php endif ?>

		<?php if ( $form ): ?>
			<div class="order-button-block">
				<a href="#post-<?php the_ID() ?>-form" class="js-open-popup-link button" data-ga-form="<?php echo $ga ?>"><?php _e( 'Заказать консультацию' ) ?></a>
			</div>
		<?php endif ?>
		
	</div>

	<div style="display:none">
		<?php if ( $form ): ?>
			<?php get_theme_part( 'section', 'popup', array(
				'data' => array(
					'id'      => 'post-'. get_the_ID() .'-form',
					'form_id' => $form,
					'title'   => get_the_title(),
					'thank'   => '
						<p class="green">Заявка на консультацию успешно оформлена.</p>
						<p>Наш менеджер свяжется с Вами <br>в ближайшее время.</p>
					',
				)
			) ) ?>
		<?php endif ?>
		<?php if ( $images ): ?>
			<?php get_theme_part( 'content/product', 'gallery-2', array(
				'data' => $images
			) ) ?>
		<?php endif ?>
	</div>


</article><?php 