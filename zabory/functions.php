<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * Dizzain theme setup
 *
 * @package WordPress
 * @subpackage Dizzain
 */

if ( !defined( 'THEME_URI' ) )
	define( 'THEME_URI', get_stylesheet_directory_uri() .'/assets' );

if ( !defined( 'THEME_INC_ABS' ) )
	define( 'THEME_INC_ABS', get_template_directory() .'/inc' );

if ( !defined( 'THEME_CACHE_GROUP' ) )
	define( 'THEME_CACHE_GROUP', 'jazeera' );

if ( !defined( 'THEME_LANG_PREFIX' ) AND defined( 'ICL_LANGUAGE_CODE' ) )
	define( 'THEME_LANG_PREFIX', '_'. ICL_LANGUAGE_CODE );

if ( !defined( 'THEME_LANG_DEFAULT' ) )
	define( 'THEME_LANG_DEFAULT', 'en' );

// add_action( 'template_redirect', 'theme_redirect' );
function theme_redirect() {
	if ( is_admin() )
		return;

	if ( is_user_logged_in() )
		return;

	wp_redirect( wp_login_url( home_url() ) );
	die;
}



/**
 * Set up theme defaults and registers support for various WordPress features.
 *
 */
add_action( 'after_setup_theme', 'theme_setup', 1 );
function theme_setup() {

	// Load up our additional theme scripts & styles
	require( THEME_INC_ABS .'/shared/lib/load-section.abstract.php' );

	// Shared theme utilities (options|meta-boxes|utils...)
	require( THEME_INC_ABS .'/shared.php' );

	// General Dizzain Style Settings and functions ( include theme scripts and styles )
	require( THEME_INC_ABS .'/general.php' );

	require( THEME_INC_ABS .'/page.php' );
	require( THEME_INC_ABS .'/post.php' );
	require( THEME_INC_ABS .'/section.php' );
	
	require( THEME_INC_ABS .'/modules.php' );

	/**
	 * Setup vital theme settings
	 */

	// Add default posts and comments RSS feed links to <head>.
	// add_theme_support( 'automatic-feed-links' );
	add_post_type_support( 'page', 'excerpt' );

	remove_action( 'wp_head', 'feed_links_extra', 3 );
	remove_action( 'wp_head', 'feed_links', 2 );
	remove_action( 'wp_head', 'wp_generator' );
	remove_action( 'wp_head', 'wlwmanifest_link' );
	remove_action( 'wp_head', 'rsd_link' );
	remove_action( 'wp_head', 'wp_shortlink_wp_head' );


	register_nav_menus( array(
		'primary'       => __( 'Primary Navigation', 'dizzain-admin' ),
	));

	// This theme uses a custom image size for featured images, displayed on "standard" posts.
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'product', 264, 176, true );
	add_image_size( 'product-2', 358, 238, true );
}

// add_action( 'admin_menu', 'theme_remove_post_menu_item' );   
function theme_remove_post_menu_item() { 
   remove_menu_page( 'edit.php' );
}


add_action( 'do_feed',      'redirect_feed_to_home', 1 );
add_action( 'do_feed_rdf',  'redirect_feed_to_home', 1 );
add_action( 'do_feed_rss',  'redirect_feed_to_home', 1 );
add_action( 'do_feed_rss2', 'redirect_feed_to_home', 1 );
add_action( 'do_feed_atom', 'redirect_feed_to_home', 1 );
function redirect_feed_to_home() {
    wp_redirect( home_url(), 301 );
}