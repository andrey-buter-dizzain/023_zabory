<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * The front page template.
 *
 *
 * @package Wordpress
 * @subpackage Dizzain
 */

get_header(); ?>

	<?php if ( have_posts() ): ?>
		<?php while( have_posts() ): the_post(); ?>
			<?php get_theme_part( 'section', 'top' ); ?>
			<?php //get_theme_part( 'section', 'advantages' ); ?>
			<?php get_theme_part( 'section', 'advantages-2' ); ?>
			<?php //get_theme_part( 'section', 'description' ); ?>
			<?php get_theme_part( 'section', 'products' ); ?>
			<?php get_theme_part( 'section', 'products-2' ); ?>
			<?php get_theme_part( 'section', 'sections' ); ?>
			<?php get_theme_part( 'section', 'how-we-work' ); ?>
		<?php endwhile ?>
	<?php endif ?>

<?php get_footer(); 