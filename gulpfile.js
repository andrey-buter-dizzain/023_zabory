const gulp = require('gulp');
// const livereload = require('gulp-livereload');
const server = require('gulp-server-livereload');

require( './gulp-tasks/theme-tasks.js' )
// require( './gulp-tasks/public-jobs-tasks.js' )

const methods = require( './gulp-tasks/methods' );

gulp.task( 'watch', ['webserver' /* for livereload */], function () {
	methods.watch.forEach( function( callback ) {
		callback();
	});
});

gulp.task('webserver', function () {
	gulp.src('themes/coople')
		.pipe(server({
			livereload: {
				enable: true,
				filter: function (filename, cb) {
					console.log(filename);
					cb(! /\.(sa|le|sc)ss$|node_modules|bower_components/.test(filename) );
				}
			},
			directoryListing: true,
			open: true,
			// port: 8001,
		}));
});