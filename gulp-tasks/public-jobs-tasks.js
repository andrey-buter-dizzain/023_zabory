const gulp = require('gulp');
const methods = require( './methods' );

const scssTask = require( './tasks/scss.js' );
const svgCssTask = require( './tasks/svg-css.js' );
const jsEs6Task = require( './tasks/js.es6.js' );
const jsTask = require( './tasks/js.js' );

const CONFIG = {
	dir: './themes/coople/components/public-jobs/assets',
	scss: 'screen.scss',
	jsEs6: 'index.js',
	jsDate: 'date-handler.js',
	src: {
		css: () => `${CONFIG.dir}/src/scss`,
		js:  () => `${CONFIG.dir}/src/js`,
		svg: () => `${CONFIG.dir}/src/svg`,
	},
	build: {
		css: () => `${CONFIG.dir}/css/`,
		js:  () => `${CONFIG.dir}/js/`,
	},
	watch: {
		scss: () =>  `${CONFIG.src.css()}/**/*.scss`,
		js:   () =>  `${CONFIG.src.js()}/**/*.js`,
	}
}

gulp.task('publicJobsCss', function () {
	return scssTask.task( `${CONFIG.src.css()}/${CONFIG.scss}`, CONFIG.build.css() );
});

gulp.task('publicJobsJsEs6', function () {
	return jsEs6Task.task( `${CONFIG.src.js()}/${CONFIG.jsEs6}`, CONFIG.build.js() );
});

gulp.task('publicJobsDateJs', function () {
	return jsTask.task( `${CONFIG.src.js()}/${CONFIG.jsDate}` , CONFIG.build.js(), CONFIG.jsDate );
});

gulp.task('publicJobsSvgInCss', function () {
	return svgCssTask.task( CONFIG.src.svg(), CONFIG.src.css() );
});

methods.addWatch(function () {
	gulp.watch( CONFIG.watch.scss(), ['publicJobsCss'] );
	gulp.watch( CONFIG.watch.js(), ['publicJobsJsEs6', 'publicJobsDateJs'] );
});