module.exports = {
	watch: [],
	getJsList: function( list ) {
		let output = [];

		for ( var path in list ) {
			output = output.concat( this.setJsPaths( path, list[ path ] ) );
		}

		return output;
	},
	setPaths: function( path, list ) {
		return list.map( ( item ) => {
			// if ( -1 !== item.indexOf( 'node_modules' ) )
			if ( 0 === item.indexOf( './' ) )
				return item;

			return path + item;
		});
	},
	addWatch( callBack ) {
		this.watch.push( callBack );
	}
	// getModuleDir: function( moduleName ) {
	// 	return modulesDir.replace( '{moduleName}', moduleName );
	// }
}