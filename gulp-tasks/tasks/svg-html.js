const gulp = require('gulp');
const svgmin = require('gulp-svgmin');
const svgSymbols  = require('gulp-svg-symbols');
const rename      = require('gulp-rename');
const imagemin = require('gulp-imagemin');

module.exports = {
	task: function( src, dest ) {
		return gulp.src( `${src}/**/*.svg`, { 
				base: process.cwd() 
			})
			.pipe(svgmin())
			.pipe(rename(function (path) {
				path.basename = "icon_" + path.basename
			}))
			.pipe(svgSymbols({
				title:      false,
				fontSize:   16,
				templates: ['default-svg']
			}))
			.pipe(rename(function (path) {
				path.dirname = `${dest}`;
				path.basename = "build-svg";
				path.extname = ".php"
			}))
			.pipe(imagemin({
				progressive: true,
				svgoPlugins: [{removeViewBox: false}]
			}))
			.pipe(gulp.dest(''));
	}
}