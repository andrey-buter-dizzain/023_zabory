const gulp = require('gulp');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const babelify = require('babelify');
const browserify = require('browserify');
const source = require('vinyl-source-stream');

module.exports = {
	task: function( src, dest, destFile = 'common.js' ) {
		return browserify({
			entries: src
		})
		.transform(babelify.configure({
		    presets : ["es2015"]
		}))
		.bundle()
		.pipe(source( destFile ))
		// .pipe(uglify())
		.pipe(gulp.dest( dest ) );
	}
}