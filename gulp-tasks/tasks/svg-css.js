const gulp = require('gulp');
const svgcss = require('gulp-svg-css');
const svgmin = require('gulp-svgmin');

module.exports = {
	task: function( src, dest, dir = 'sprites' ) {
		return gulp
			.src( `${src}/**/*.svg` )
			.pipe(svgmin())
			.pipe(svgcss({
				cssPrefix: 'icon-svg-',
				addSize: true,
				fileExt: 'scss',
				fileName: 'svg-to-css',
			}))
			.pipe( gulp.dest( `${dest}/${dir}/` ) );
	}
}