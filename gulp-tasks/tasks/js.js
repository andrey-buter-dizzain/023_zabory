const gulp = require('gulp');
// const mergeStream = require('merge-stream');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
// const pump = require('pump');
const babel = require('gulp-babel');

module.exports = {
	task: function( src, dest, destFile = 'common.js' ) {
		console.log( src);
		console.log( dest);
		return gulp.src( src )
			.pipe(concat( destFile ))
			.pipe(babel({
				presets: ['es2015']
			}))
			// .pipe(uglify())
			.pipe(gulp.dest( dest ) );
	}
}