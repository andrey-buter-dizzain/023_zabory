const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const livereload = require('gulp-livereload');

module.exports = {
	task: function( src, dest ) {
		return gulp.src( src )
			// .pipe(sourcemaps.init())
			.pipe(sass().on('error', sass.logError))
			.pipe(autoprefixer({
				browsers: ['last 2 versions']
			}))
			// .pipe(sourcemaps.write())
			.pipe(gulp.dest( dest ))
			.pipe(livereload());
	}
}
