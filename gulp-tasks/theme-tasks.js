const gulp = require('gulp');
const methods = require( './methods' );

const scssTask = require( './tasks/scss.js' );
const svgHtmlTask = require( './tasks/svg-html.js' );
const jsEs6Task = require( './tasks/js.es6.js' );
const jsTask = require( './tasks/js.js' );

const CONFIG = {
	dir: './zabory/assets',
	scss: 'screen.scss',
	jsLibs: [
		'/../bower_components/responsive-bootstrap-toolkit/dist/bootstrap-toolkit.min.js',
		'/../bower_components/jquery-touchswipe/jquery.touchSwipe.min.js',
		'/../bower_components/magnific-popup/dist/jquery.magnific-popup.js',
		'/../bower_components/owl.carousel/dist/owl.carousel.min.js',
		'/../bower_components/ScrollIt/scrollIt.js',
		'/../bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js',
		// './node_modules/knockout.validation/dist/knockout.validation.js',
		// '/libs/sticky-kit/sticky-kit.js',
	],
	jsEs6:'index.js',
	jsConcat: [
		'libs.js',
		'theme.js',
	],
	src: {
		css: () => `${CONFIG.dir}/src/scss`,
		js:  () => `${CONFIG.dir}/src/js`,
		svg: () => `${CONFIG.dir}/src/svg`,
	},
	build: {
		css: () => `${CONFIG.dir}/css/`,
		js:  () => `${CONFIG.dir}/js/`,
		svg: () => `${CONFIG.dir}/images/`,
	},
	watch: {
		scss: () => `${CONFIG.src.css()}/**/*.scss`,
		js:   () => `${CONFIG.src.js()}/**/*.js`,
		svg:  () => `${CONFIG.src.svg()}/**/*.svg`,
	}
}

gulp.task( 'themeStyles', function () {
	return scssTask.task( `${CONFIG.src.css()}/${CONFIG.scss}`, CONFIG.build.css() );
} );

gulp.task( 'themeJsLibs', function () {
	return jsTask.task( methods.setPaths( CONFIG.src.js(), CONFIG.jsLibs ), CONFIG.build.js(), 'libs.js' );
} );


gulp.task( 'themeJsEs6', function () {
	return jsEs6Task.task( `${CONFIG.src.js()}/${CONFIG.jsEs6}`, CONFIG.build.js() /* , 'theme.js' */ );
} );

gulp.task( 'themeJsConcat', function () {
	// return jsTask.task( methods.setPaths( CONFIG.build.js(), CONFIG.jsConcat ), CONFIG.build.js(), 'common.js' );
} );

gulp.task( 'themeSvgHtml', function () {
	return svgHtmlTask.task( CONFIG.src.svg(), CONFIG.build.svg() );
} );

methods.addWatch(function () {
	gulp.watch( CONFIG.watch.scss(), [ 'themeStyles' ] );
	gulp.watch( CONFIG.watch.js(), [ 'themeJsLibs', 'themeJsEs6', /* 'themeJsConcat' */ ] );
	// gulp.watch( CONFIG.watch.svg(),  ['themeSvgHtml'] );
});